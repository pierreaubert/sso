#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
from aclex import AC_Lex
from ac import AC_And, AC_Or, AC_Not, AC_IP, AC_Group, AC_UserSelf, \
    AC_UserGroup
# lex/yacc
import ply.yacc as lalr

class AC_Yacc:

    def __init__(self):
        self.tokens = AC_Lex.tokens
        self.lexer = AC_Lex().lexer()
        self.yacc = lalr.yacc(module=self)
        #self.yacc = lalr.yacc(optimize=1, module=self)

    def p_error(self, p):
        # print 'DEBUG: ' + str(p)
        pass

    def _error(self, p, msg):
        # print 'DEBUG: ' + msg
        self.p_error(p)

    def p_expr(self, p):
        '''
        expr : pexpr AND pexpr
             | pexpr OR pexpr
             | NOT pexpr
             | term
        '''
        # print 'expr='+str(len(p))
        if len(p) == 4:
            bin_op = p[2]
            if 'and' == bin_op:
                p[0] = AC_And(p[1], p[3])
            elif 'or' == bin_op:
                p[0] = AC_Or(p[1], p[3])
            else:
                self._error(p, 'unkown binary operator "'+bin_op+'"')
        elif len(p) == 3:
            una_op = p[1]
            if 'not' == una_op:
                p[0] = AC_Not(p[2])
            else:
                self._error(p, 'unkown unary operator "'+una_op+'"')
        elif len(p) == 2:
            p[0] = p[1]
        else:
            self._error(p, 'p_expr get '+str(len(p))+' parameters')

    def p_pexpr(self, p):
        '''
        pexpr : LPAREN expr RPAREN
        '''
        # print 'pexpr='+str(len(p))
        if len(p) == 4:
            p[0] = p[2]
        else:
            self._error(p, 'p_pexpr get '+str(len(p))+' parameters')

    def p_term(self, p):
        '''
        term : ip
             | username
             | group
             | usergroups
        '''
        # print 'term='+str(len(p))
        p[0] = p[1]

    def p_username(self, p):
        '''
        username : USERNAME LPAREN quote RPAREN
        '''
        # print 'username='+str(len(p))
        p[0] = AC_UserSelf(p[3])

    def p_ip(self, p):
        '''
        ip : IP LPAREN SQUOTE IPV4 SQUOTE RPAREN
        '''
        # print 'ip='+str(len(p))
        p[0] = AC_IP(p[4])

    def p_group(self, p):
        '''
        group : GROUPNAME LPAREN quote RPAREN
        '''
        # print 'group='+str(len(p))
        if len(p) == 5:
            p[0] = AC_Group(p[3])
        else:
            self._error(p, 'p_group get '+str(len(p))+' parameters')

    def p_usergroups(self, p):
        '''
        usergroups : USERGROUPS LPAREN SET LPAREN LBRAC list RBRAC RPAREN RPAREN
        '''
        # print 'usergroups='+str(len(p))
        if len(p) == 10:
            p[0] = AC_UserGroup(p[6])
        else:
            self._error(p, 'p_usergroups get '+str(len(p))+' parameters')

    def p_list(self, p):
        '''
        list : quote COMMA list
             | quote
        '''
        # print 'list='+str(len(p))
        if len(p) == 4:
            p[0] = (p[1], p[3])
        elif len(p) == 2:
            p[0] = p[1]
        else:
            self._error(p, 'p_list get '+str(len(p))+' parameters')

    def p_quote(self, p):
        '''
        quote : SQUOTE NAME SQUOTE
        '''
        # print 'quote='+str(len(p))
        p[0] = p[2]


