#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from functools import wraps
from flask import current_app, g, request
from sso.client.rest import Client
from sso.accesscontrol.exceptions import Forbidden, Unauthorized


def forward_ssosign_cookie(response):
    """ the sso is taking care of authentification, if the cookie has changed, 
    we need to copy it from requests session to flask response 
    """
    # did we built the sso client?
    if 'sso_client' in g:
        # do we have a cookies?
        if len(g.sso_client.cookies)>0:
            # do we have our auth cookie?
            if 'ssosign' in g.sso_client.cookies:
                # then set it up
                response.set_cookie('ssosign', g.sso_client.cookies['ssosign'])
    return response
        

def authenticate(func):
    """ this decorator enforce authentification 
        if the user is authenticated it calls the decorated function (func),
        if not it raise an Unauthorize exception
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        """ inner wrapped function
        """
        log = current_app.logger
        #trace log.info('(authenticate): start')
        if 'userinfo' not in g:
            #trace log.info('(authenticate): g.userinfo is not set')
            if 'ssosign' not in request.cookies:
                #trace log.info('(authenticate): no cookie, raising 401')
                raise Unauthorized('No cookie')

            # do we have a sso client?
            if 'sso_client' not in g:
                g.sso_client = Client(current_app.config['SSO_URL'], log)

            # set cookie from the current flask request to the requests session
            # which is used inside the sso_client
            if request:
                # there is a bug in some version of requests
                # see: https://github.com/kennethreitz/requests/issues/1189
                if 'ssosign' in g.sso_client.cookies:
                    g.sso_client.cookies['ssosign'] = None
                g.sso_client.cookies['ssosign'] = request.cookies['ssosign']

            # user is authenticated, thus call func (usually a view)
            #trace log.info('(authenticate): we have a cookie, check if valid')
            g.userinfo = g.sso_client.get_profile()
            if g.userinfo is None:
                #trace log.info('(authenticate): cookie is invalid, raising 401')
                raise Unauthorized('Invalid cookie')
        #trace else:
            #trace log.info('(authenticate): g.userinfo is set too: {0}'.format(g.userinfo))

        if 'userinfo' not in g:
            log.error('Wrapper bug: userinfo not in g but calling func')

        # user is authenticated, thus call func (usually a view)
        #trace log.info('(authenticate): end')
        return func(*args, **kwargs)
    return wrapper


def allow(access_control):
    """ this decorator enforce authentification and checked that the current 
    authenticated user has the required credentials to access the function
    it expect the find the sso client under current_app.sso_client
    """
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            """ inner wrapped function
            """
            log = current_app.logger
            # do we have a sso client?
            if 'sso_client' not in g:
                g.sso_client = Client(
                    current_app.config['SSO_URL'],
                    current_app.logger)

            # do we already have authent info?
            if 'userinfo' not in g:
                #trace log.info('(allow): g.userinfo is not set')
                if 'ssosign' not in request.cookies:
                    #trace log.info('(allow): no cookie, raising 401')
                    raise Unauthorized('No cookie')
                
                # set cookie from the current flask request to the requests session
                # which is used inside the sso_client
                if request:
                    # there is a bug in some version of requests
                    # see: https://github.com/kennethreitz/requests/issues/1189
                    if 'ssosign' in g.sso_client.cookies:
                        g.sso_client.cookies['ssosign'] = None
                    g.sso_client.cookies['ssosign'] = request.cookies['ssosign']

                # user is authenticated, thus call func (usually a view)
                #trace log.info('(allow): we have a cookie, check if valid')
                g.userinfo = g.sso_client.get_profile()
                if g.userinfo is None:
                    #trace log.info('(allow): cookie is invalid, raising 401')
                    raise Unauthorized('Invalid cookie')
            #trace else:
                #trace log.info('(allow): g.userinfo is set too: {0}'.format(g.userinfo))
                
            # be carefull
            if 'userinfo' not in g:
                current_app.logger.error('Wrapper bug: userinfo not in g but calling func')
                raise Forbidden()

            # be carefull
            if 'sso_client' not in g:
                current_app.logger.error('Wrapper bug: userinfo is set but no sso_client')
                raise Forbidden()

            # do we have rules in g?
            if 'rules' not in g:
                g.rules = {}

            # check access control via cached rules
            #trace log.info('(allow): call rule {0}'.format(access_control))
            ac = str(access_control)
            if ac in g.rules:
                status = g.rules[ac]
                if status:
                    #trace log.info('(allow): rules already checked all clear, calling func')
                    return func(*args, **kwargs)
                else:
                    #trace log.info('(allow): rules already checked all clear, forbidden')
                    raise Forbidden()
                    
            # check access control, this make a rest call
            if not g.sso_client.is_allowed(access_control):
                #trace log.info('(allow): raising forbidden')
                g.rules[ac] = False
                raise Forbidden()
            else:
                # user is authenticated, thus call func (usually a view)
                #trace log.info('(allow): all clear, calling func')
                g.rules[ac] = True
                return func(*args, **kwargs)
        return wrapper
    return decorator
