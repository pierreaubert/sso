#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------


import logging
from datetime import date

# version
VERSION = "0.11"

# debug is false by default
DEBUG = True

# testing is false by default
TESTING = False

# your favorite database, sqlalchimy convention
#DATABASE_URI = 'sqlite:////Users/pierre/Run/data/sqlite_dev/sso.db'
DATABASE_URI='postgresql+psycopg2://sso:gargamellekkksso@localhost/sso'

# main log file
LOG_APP =  "LOG"
LOG_FILE= "/Users/pierreaubert/Run/var/log/sso_dev_{0}.log".format(date.today().isoformat())
LOG_LEVEL= logging.DEBUG
#LOG_LEVEL= logging.INFO

# host/port for sso
SSO_PORT=9080
SSO_HOST="0.0.0.0"

# local sentry (let empty if you do not use sentry)
SENTRY_DSN=''
#SENTRY_DSN='http://e91b98f4076d4547be5dd5ae09dcdd16:c4917a56757546a78566edd649827267@sentry.7pi.eu:9000/2'

# ----------------------------------------------------------------------
# MUST BE EDITED
# ----------------------------------------------------------------------

# SECRET KEY for debug and dev
SECRET_KEY='dev&debug&are&fun'

# admin user
ADMIN_USERNAME='admin'
ADMIN_PASSWORD='admin2013'
ADMIN_EMAIL='admin@localhost'

# ----------------------------------------------------------------------
# END MUST BE EDITED
# ----------------------------------------------------------------------





