#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

import logging

VERSION='0.2-tests'
DEBUG = True
TESTING = True
LOG_LEVEL = logging.DEBUG
LOG_APP =  "sso_test"
LOG_FILE= "/Users/pierreaubert/Run/var/log/sso.log"
SSO_PORT=9090
SSO_HOST="0.0.0.0"
SENTRY_DSN=''
DATABASE_URI = 'sqlite:////Users/pierreaubert/Run/data/sqlite_dev/tests_sso.db'
SECRET_KEY='dev&debug&are&fun'
ADMIN_USERNAME='admin'
ADMIN_PASSWORD='admin2013'
ADMIN_EMAIL='admin@localhost'




    



