#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
from flask import jsonify, current_app, request, g
from flask.views import MethodView

# get models
from models import Users, Followers, Groups, UsersGroups

# get helpers
from helpers import sso_db_get_conn, json_status, get_caller_ip

# get authent/access control
from accesscontrol.ac import AC_UserGroup, AC_Or, AC_UserSelf
from accesscontrol.wrappers import \
    inner_authenticate as authenticate, \
    inner_allow as allow, is_allowed
from accesscontrol.exceptions import Unauthorized, Forbidden, \
    BadRequest, NotFound
from sso.accesscontrol.parser import AC_Parser


class LoginAPI(MethodView):
    """ implement the login API in a restfull manner
    """

    @authenticate
    def get(self):
        """ return ok in json if user is authenticated; ko if not
        """
        # force cookie update
        return json_status(None, True, g.cookie)

    def post(self):
        """ log in and create cookie

        this method is not protected, it's access should be limited with a captcha
        """
        error = None
        cookie = None
        caller_ip = get_caller_ip(request)
        db_conn = sso_db_get_conn()
        db_users = Users(db_conn)

        # allow arguments to be in a form or in a json field
        jdata = None
        if 'Content-Type' in request.headers and \
           'json' in request.headers['Content-Type']:
            jdata = request.get_json()
        else:
            jdata = request.form

        if 'username' not in jdata:
            raise Unauthorized('No username')
        elif 'password' not in jdata:
            raise Unauthorized('No password')
        username = jdata['username']
        password = jdata['password']

        # check short/long password
        short = True
        if 'longtime' in jdata:
            short = False

        # get username, pwd from database
        user = db_users.read(username)
        if user is None:
            raise Unauthorized('Invalid username or password')

        password_hash = user['password_hash']
        if not db_users.is_password_valid(password_hash, password):
            raise Unauthorized('Invalid password or password')
            
        # print username, caller_ip, short
        cookie = current_app.cm.create(username, caller_ip, short)

        # force update of cookie
        return json_status(error, True, cookie)

    def delete(self):
        """ delete authent cookie

        post condition:
          GET / will return false after the call
        """
        error = None
        # force delete of cookie
        return json_status(error, True, None)


class GroupsAPI(MethodView):
    """
        Implement the group API in a restfull manner

    GET return information about a group
    PUT modify a group
    POST create a group
    DELETE delete a group (removed too dangerous, do it directly
           in the database)
    You must be authenticated with a sufficient level to get access
    """
    @authenticate
    def get(self, groupname=None):
        """
        return the profile for an authenticated user
        pre condition: must be authenticated in group users
        """
        db_conn = sso_db_get_conn()
        db_groups = Groups(db_conn)
        group = db_groups.read(groupname)
        if group is not None:
            resp = {
                'status': 'ok',
                'groups': group
            }
            return jsonify(resp)

        return NotFound('group {0} do not exist'.format(groupname))

    @allow(AC_UserGroup('admin'))
    def post(self, groupname, parent_groupname=None):
        """ create a group
        pre condition: must be authenticated in group admin
        """
        db_conn = sso_db_get_conn()
        db_groups = Groups(db_conn)
        # default group name is user
        parent_id = None
        if parent_groupname is not None:
            parent_id = Groups(db_conn).id(parent_groupname)
            if parent_id is None:
                return NotFound('paren group {0} do not exist'.\
                                format(parent_groupname))

        db_groups.create(groupname, parent_id)
        resp = {
            'status': 'ok'
        }
        return jsonify(resp)

    @allow(AC_UserGroup('admin'))
    def put(self, groupname, parent_groupname):
        """
        modify parent of a group
        pre condition: must be authenticated in group admin
        """
        db_conn = sso_db_get_conn()
        db_groups = Groups(db_conn)
        # default group name is user
        group_id = Groups(db_conn).id(groupname)
        if group_id is None:
            raise NotFound('Group {} does not exist'.format(groupname))

        parent_id = Groups(db_conn).id(parent_groupname)
        if parent_id is None:
            return NotFound('paren group {0} do not exist'.\
                            format(parent_groupname))

        db_groups.update(group_id, parent_id)
        resp = {
            'status': 'ok'
        }
        return jsonify(resp)


class UsersGroupsAPI(MethodView):
    """
    implement the relationship between group and user
    """
    def get(self, username):
        """ return all groups for a user
        """
        valid, g.cookie, error, g.userinfo = is_allowed(
            request,
            AC_Or(
                AC_UserSelf(username),
                AC_UserGroup('admin')))
        if valid:
            db_conn = sso_db_get_conn()
            db_usersgroups = UsersGroups(db_conn)
            usergroups = db_usersgroups.read(username)
            if usergroups is not None:
                resp = {
                    'status': 'ok',
                    'username': username,
                    'usergroups': usergroups
                }
                return jsonify(resp)
            else:
                return NotFound('User {0} has no group'.format(username))
        else:
            return Forbidden(error)

    def post(self, username, groupname):
        """ add group to user
        """
        valid, g.cookie, error, g.userinfo = is_allowed(
            request,
            AC_Or(
                AC_UserSelf(username),
                AC_UserGroup('admin')))
        if valid:
            db_conn = sso_db_get_conn()
            db_usergroups = UsersGroups(db_conn)
            error = db_usergroups.create(username, groupname)
            return json_status(error, False)
        else:
            return Forbidden(error)

    def put(self, username, parent_groupname):
        """ idem post
        """
        return self.post(username, parent_groupname)

    @allow(AC_UserGroup('admin'))
    def delete(self, username, groupname):
        """ delete a group from user's list of groups
        """
        db_conn = sso_db_get_conn()
        db_usergroups = UsersGroups(db_conn)
        error = db_usergroups.delete(username, groupname)
        return json_status(error, False)


class ProfileAPI(MethodView):
    """ implement the profile API in a restfull manner
    GET return information about user (must be authenticated)
    PUT modify a user
    POST create a user
    DELETE delete a user (must be authenticated)
    """
    @authenticate
    def get(self):
        """ return the profile for an authenticated user
        pre condition:
          must be authenticated
        """
        db_conn = sso_db_get_conn()
        db_users = Users(db_conn)
        profile = db_users.read_public(g.userinfo['username'])
        if profile is not None:
            resp = {
                'status': 'ok',
                'users': profile
            }
            # should we update the cookie?
            return jsonify(resp)
        else:
            error = 'Unknown user'

        return json_status(error, False)

    def post(self):
        """ create a user """
        db_conn = sso_db_get_conn()
        db_users = Users(db_conn)
        # default group name is user
        groupname = 'users'

        jdata = None
        if 'Content-Type' in request.headers and 'json' in request.headers['Content-Type']:
            jdata = request.get_json()
        else:
            jdata = request.form

        if 'username' not in jdata:
            raise BadRequest('You have to enter a username')

        if len(jdata['username']) < 6:
            raise BadRequest('Username too short (min 6 characters)')

        if len(jdata['username']) >= 16:
            raise BadRequest('Username too long (max 16 characters)')

        if 'password' not in jdata:
            raise BadRequest('You have to enter a password')

        if 'password2' not in jdata or \
           jdata['password'] != jdata['password2']:
            raise BadRequest('The two passwords do not match')

        if db_users.id(jdata['username']) is not None:
            raise BadRequest('The username is already taken')

        if db_users.email(jdata['email']) is not None:
            raise BadRequest('This email is already registered')
            
        if 'groupname' in jdata:
            groupname = jdata['groupname']

        group_id = Groups(db_conn).id(groupname)
        if group_id is None:
            raise BadRequest('Group {} does not exist'.format(groupname))

        db_users.create(jdata['username'],
                        jdata['password'],
                        jdata['email'],
                        group_id)
            
        return json_status(None, False)

    def delete(self, username):
        """ delete a user profile
        """
        valid, g.cookie, error, g.userinfo = is_allowed(
            request,
            AC_Or(
                AC_UserSelf(username),
                AC_UserGroup('admin')))
        if username == 'admin':
            error = 'cannot delete admin user'
        if valid and (error is None):
            db_conn = sso_db_get_conn()
            db_users = Users(db_conn)
            # ok now delete user
            user = db_users.read(username)
            user_id = user['user_id']
            db_users.delete(user_id)

            # delete user & authent
            return json_status(error, True, None)
        else:
            raise Forbidden()


class FollowersAPI(MethodView):
    """ who is following whom

    GET /follow/out               list of username
    GET /follow/in                list of username
    POST /follow                  add a follower to username

    Example:
              Follow Table
         --------------------
         U1       ->       U2
         U1       ->       U3
         U2       ->       U3
         U3       ->       U1

    /in for U1 returns [U3]
    /out for U1 returns [U2, U3]
    """

    @authenticate
    def get(self):
        """
        """
        db_conn = sso_db_get_conn()
        # get id for current user
        db_users = Users(db_conn)
        username = g.userinfo['username']
        user_id = db_users.id(username)
        # get list
        lof = []
        path = None
        if request.path == '/followers/in':
            lof = Followers(db_conn).read_graph_in(user_id)
            path = 'in'
        elif request.path == '/followers/out':
            lof = Followers(db_conn).read_graph_out(user_id)
            path = 'out'

        l = [f[0] for f in lof]
        dict = {
            'status': 'ok',
            'graph': {
                'node': username,
                path: l
            }
        }
        return jsonify(dict)

    @authenticate
    def post(self, followname):
        """
        """
        db_conn = sso_db_get_conn()
        # get id for current user
        db_users = Users(db_conn)
        username = g.userinfo['username']
        user_id = db_users.id(username)
        # get id for follow user
        follower_id = db_users.id(followname)
        error = None
        if follower_id is None:
            error = 'unknown user ({0})'.format(followname)
        else:
            # insert
            error = Followers(db_conn).create(user_id, follower_id)
        return json_status(error, False, g.cookie)

    @authenticate
    def delete(self, followname):
        """
        """
        db_conn = sso_db_get_conn()
        db_users = Users(db_conn)
        username = g.userinfo['username']
        user_id = db_users.id(username)
        follower_id = db_users.id(followname)
        error = None
        if follower_id is not None:
            error = Followers(db_conn).delete(user_id, follower_id)
        return json_status(error, False, g.cookie)


class AccessControlAPI(MethodView):
    """ implement the accesscontrol API
    """
    def __init__(self):
        """ build the parser 

        BUG: should use optimized in order not to regenerate table
        """
        self.lalr = AC_Parser()

    def _check(self, serialized):
        """ check current user validate the rule """
        ac = self.lalr.parse(serialized)
        if ac is None:
            raise BadRequest('Cannot parser ac')

        if not ac.is_allowed(g.userinfo):
            return Forbidden()

        return json_status(None, False, g.cookie)

    @authenticate
    def get(self):
        """ check rule 
        GET /accesscontrol?ac=rule
        """
        jdata = request.args.get('ac')
        return self._check(jdata)

    @authenticate
    def post(self):
        """ check rule
        POST /accesscontrol
        """
        jdata = None
        if 'Content-Type' in request.headers and 'json' in request.headers['Content-Type']:
            jdata = request.get_json()
        else:
            jdata = request.form
        if 'ac' not in jdata:
            raise BadRequest('''no 'ac' in parameters''')

        return self._check(jdata['ac'])

        

