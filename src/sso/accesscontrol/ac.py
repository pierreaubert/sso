#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
# get models
from sso.models import Groups, UsersGroups

# get helpers
from sso.helpers import sso_db_get_conn

class AccessConstraint(object):
    """ a AccessConstraint is used to check if a user has access to a
    ressource
    """
    def __init__(self):
        pass

    def __repr__(self):
        return 'ac()'

    def __str__(self):
        return '()'

    def is_allowed(self, user):
        return False


class AC_User(AccessConstraint):
    """ this constraint is valid if current user match the constraint
    on user
    """
    def __init__(self, username):
        self.ac = username
        self.constraints = 'username'

    def __repr__(self):
        return """{!r}('{!r}')""".format(self.constraints, self.ac)

    def __str__(self):
        return """{!s}('{!s}')""".format(self.constraints, self.ac)

    def is_allowed(self, user):
        if self.constraints in user:
            return self.ac == user[self.constraints]
        return False


class AC_UserSelf(AccessConstraint):
    """ this constraint is valid if current user match the declare user
    """
    def __init__(self, username):
        self.ac = username
        self.constraints = 'username'

    def __repr__(self):
        return """{!r}('{!r}')""".format(self.constraints, self.ac)

    def __str__(self):
        return """{!s}('{!s}')""".format(self.constraints, self.ac)

    def is_allowed(self, user):
        if self.constraints in user:
            return self.ac == user[self.constraints]
        else:
            print 'error in AC on user'
            print user
        return False


class AC_IP(AccessConstraint):
    """ this constraint is valid if current user's IP match the allowed IP
    TODO: use ipaddress in python 3.X
    """
    def __init__(self, ipv4):
        self.ac = ipv4
        self.constraints = 'ip'

    def __repr__(self):
        return """{!r}('{!r}')""".format(self.constraints, self.ac)

    def __str__(self):
        return """{!s}('{!s}')""".format(self.constraints, self.ac)

    def is_allowed(self, user):
        if self.constraints in user:
            return self.ac == user[self.constraints]
        return False


class AC_Group(AccessConstraint):
    """ this constraint is valid if the test group match the constraint
    group or one of his parent
    """
    def __init__(self, groupname):
        self._init = False
        self.constraints = 'groupname'
        self.groupname = groupname

    def __repr__(self):
        if not self._init:
            self._build()
        return """{!r}('{!r}')""".format(self.constraints, self.ac)

    def __str__(self):
        if not self._init:
            self._build()
        return """{!s}('{!s}')""".format(self.constraints, self.ac)

    def _build(self):
        self.db_conn = sso_db_get_conn()
        self.db_groups = Groups(self.db_conn)
        self.ac = set(group for group in
                      self.db_groups.children(self.groupname))

    def is_allowed(self, user):
        if not self._init:
            self._build()
        if self.constraints in user:
            parents = set(group for group in
                          self.db_groups.parents(user[self.constraints]))
            return len(parents.intersection(self.ac)) > 0
        return False


class AC_UserGroup(AccessConstraint):
    """ this constraint is valid if current user is in a group which
    match the role group
    """
    def __init__(self, groupname):
        self._init = False
        self.constraints = 'usergroups'
        self.groupname = groupname

    def __repr__(self):
        if not self._init:
            self._build()
        return """{!r}({!r})""".format(self.constraints, self.ac)

    def __str__(self):
        if not self._init:
            self._build()
        return """{!s}({!s})""".format(self.constraints, self.ac)

    def _build(self):
        """
        """
        self.db_conn = sso_db_get_conn()
        self.db_groups = Groups(self.db_conn)
        self.db_usergroups = UsersGroups(self.db_conn)
        # for example {admin,admin_bd}
        self.ac = set(self.db_groups.children(self.groupname))
        #print 'self.ac'
        #print self.ac
        self._init_ = True

    def is_allowed(self, user):
        if not self._init:
            self._build()
        # test on 'username' and not on self.constraints
        if 'username' in user:
            # get list of groups from user
            # for example {bd, user}
            usergroups = self.db_usergroups.read(user['username'])
            # merge them with ancestors
            # for example {bd, user, admin_bd}
            all_groups = [self.db_groups.parents(groups['groupname'])
                          for groups in usergroups]
            # flatten then apply an uniq filter
            flat = sum(all_groups, [])
            ac = set(group for group in flat)
            # is current user group in the list?
            # for example {bd, user, admin_bd} intersect {admin,admin_bd}
            #print 'list of groups for user: ' + user['username']
            #print ac
            #print 'match group:'
            #print self.ac
            return len(self.ac.intersection(ac)) > 0
        return False


class AC_And(AccessConstraint):
    """ this constraint is valid if both subconstraints are valid
    the role group
    """
    def __init__(self, ac1, ac2):
        self.ac1 = ac1
        self.ac2 = ac2

    def __repr__(self):
        return '({0!r})and({1!r})'.format(self.ac1, self.ac2)

    def __str__(self):
        return '({0!s})and({1!s})'.format(self.ac1, self.ac2)

    def is_allowed(self, constraint):
        return self.ac1.is_allowed(constraint) \
            and self.ac2.is_allowed(constraint)


class AC_Or(AccessConstraint):
    """ this constraint is valid if 1 subconstraint is valid
    """
    def __init__(self, ac1, ac2):
        self.ac1 = ac1
        self.ac2 = ac2

    def __repr__(self):
        return '({0!r})or({1!r})'.format(self.ac1, self.ac2)

    def __str__(self):
        return '({0!s})or({1!s})'.format(self.ac1, self.ac2)

    def is_allowed(self, constraint):
        return self.ac1.is_allowed(constraint) or \
            self.ac2.is_allowed(constraint)


class AC_Not(AccessConstraint):
    """ this constraint is the opposite of the contraint
    """
    def __init__(self, ac):
        self.ac = ac

    def __repr__(self):
        return 'not({0!r})'.format(self.ac)

    def __str__(self):
        return 'not({0!s})'.format(self.ac)

    def is_allowed(self, constraint):
        return not self.ac.is_allowed(constraint)


# simplify import
__all__ = [AC_User, AC_UserSelf, AC_IP, AC_UserSelf, 
           AC_And, AC_Or, AC_Not, AccessConstraint]

