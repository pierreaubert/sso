#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
import unittest
from sso.accesscontrol.ac import AC_User
from sso.client.decorators.dflask import authenticate, allow
from contextlib import contextmanager
from flask import Flask, appcontext_pushed, g
import httpretty

sso_url = 'http://localhost:8092' 
app = Flask('decorators')
app.config['SSO_URL'] = sso_url

@app.route('/auth')
@authenticate
def auth():
    # print 'Authorized: {0}'.format(g.userinfo)
    return 'logged'


@app.route('/allow')
@allow(AC_User('admin'))
def allow():
    # print 'Allowed: {0}'.format(g.userinfo)
    return 'allowed'


@contextmanager
def mock_set_user(app, user_id):
    """ create a fake user_id
    here we bypass the check in sso.client.decorators.dflask
    """
    def handler(sender, **kwargs):
        """ store the fake id in g """
        g.userinfo = {
            'user_id': user_id
        }
        if user_id == 1:
            g.userinfo['username'] = 'admin'
        else:
            g.userinfo['username'] = 'user'
    with appcontext_pushed.connected_to(handler, app):
        yield


class FlaskDecoratorsTests(unittest.TestCase):

    def setUp(self):
        self.server = app
        self.server.testing = True

    def tearDown(self):
        pass

    def test_auth_decorator(self):
        """ mock a ok easy way via faking an already authenticated customer """
        with mock_set_user(self.server, 1):
            with self.server.test_client() as tc:
                status = tc.get('/auth')
                self.assertEqual(200, status.status_code)

    def test_auth_decorator_no_g(self):
        """ expect a ko, no cookie """
        with self.server.test_client() as tc:
            status = tc.get('/auth')
            self.assertEqual(401, status.status_code)

    @httpretty.activate
    def test_auth_decorator_no_valid_cookie(self):
        """ expect a ko, invalid cookie """
        with self.server.test_client() as tc:
            # add a fake cookie
            cookie = ('Cookie', 'ssosign=wrong; Expires=Wed, 29-Jan-2014 09:08:08 GMT; Max-Age=900; Path=/')
            headers = [cookie]
            # fake answer from sso server
            httpretty.register_uri(
                httpretty.GET,
                sso_url + '/profile',
                body='{"status":"ko", "error": "invalid cookie"}',
                status=401)
            # call test app
            status = tc.get('/auth', headers=headers)
            self.assertEqual(401, status.status_code)

    @httpretty.activate
    def test_auth_decorator_valid_cookie(self):
        """ expect a ko, invalid cookie """
        with self.server.test_client() as tc:
            # add a fake cookie
            cookie = ('Cookie', 'ssosign=ok; Expires=Wed, 29-Jan-2014 09:08:08 GMT; Max-Age=900; Path=/')
            headers = [cookie]
            # fake answer from sso server
            httpretty.register_uri(
                httpretty.GET,
                sso_url + '/profile',
                body='{"status":"ok", "users": {"user_id":1, "username": "user"}}',
                status=200)
            # call test app
            status = tc.get('/auth', headers=headers)
            self.assertEqual(200, status.status_code)

    @httpretty.activate
    def test_allow_decorator(self):
        """ mock a ok easy way via faking an already authenticated customer """
        with mock_set_user(self.server, 1):
            with self.server.test_client() as tc:
                httpretty.register_uri(
                    httpretty.POST,
                    sso_url + '/accesscontrol',
                    body='{"status":"ok"}',
                    status=200)
                status = tc.get('/allow')
                self.assertEqual(200, status.status_code)

    def test_allow_decorator_no_g(self):
        """ expect a ko, no cookie """
        with self.server.test_client() as tc:
            status = tc.get('/allow')
            self.assertEqual(401, status.status_code)

    @httpretty.activate
    def test_allow_decorator_no_valid_cookie(self):
        """ expect a ko, invalid cookie """
        with self.server.test_client() as tc:
            # add a fake cookie
            cookie = ('Cookie', 'ssosign=wrong; Expires=Wed, 29-Jan-2014 09:08:08 GMT; Max-Age=900; Path=/')
            headers = [cookie]
            # fake answer from sso server
            httpretty.register_uri(
                httpretty.GET,
                sso_url + '/profile',
                body='{"status":"ko", "error": "invalid cookie"}',
                status=401)
            # call test app
            status = tc.get('/allow', headers=headers)
            self.assertEqual(401, status.status_code)

    def test_allow_decorator_valid_rule(self):
        """ expect a ko, invalid cookie """
        with self.server.test_client() as tc:
            # add a fake cookie
            cookie = ('Cookie', 'ssosign=ok; Expires=Wed, 29-Jan-2014 09:08:08 GMT; Max-Age=900; Path=/')
            headers = [cookie]
            # fake answer(s) from sso server
            httpretty.enable()
            # first call faked cookies check
            httpretty.register_uri(
                httpretty.GET,
                sso_url + '/profile',
                body='{"status":"ok", "users": {"user_id":1, "username": "user"}}',
                status=200)
            # first call faked rule acceptance
            httpretty.register_uri(
                httpretty.POST,
                sso_url + '/accesscontrol',
                body='{"status":"ok"}',
                status=200)
            # call test app
            status = tc.get('/allow', headers=headers)
            self.assertEqual(200, status.status_code)
            httpretty.disable()


if __name__ == '__main__':
    unittest.main()


