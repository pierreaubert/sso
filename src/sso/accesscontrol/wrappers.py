#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
from functools import wraps
from flask import current_app, request, g

# get helpers
from sso.helpers import get_caller_ip

# get exception
from sso.accesscontrol.exceptions import Unauthorized, Forbidden


def is_authenticated(current_request):
    """
    a user is logged if username is defined in cookie(ssosign)
    return true/false, cookie (if valid), error msg (if invalid), datas if any
    """
    error = None
    caller_ip = get_caller_ip(current_request)
    cookie = None
    # a user is logged if username is defined in cookie(ssosign)
    if 'ssosign' not in current_request.cookies:
        error = 'no cookie'
        return False, None, error, None
    else:
        valid, cookie, msg, userinfo = current_app.cm.is_valid_verbose(
            current_request.cookies['ssosign'],
            caller_ip)
        if not valid:
            error = msg
        #else:
        #    print '{0}: {1}'.format(cookie, userinfo)
        return valid, cookie, error, userinfo


def is_allowed(current_request, access_control):
    """
    return true if the current user is allowed to access the protected
           ressource
    return false otherwise
    """
    valid, cookie, error, userinfo = is_authenticated(current_request)
    if valid:
        # check if user is allowed
        # print userinfo
        # print(access_control)
        valid = access_control.is_allowed(userinfo)
        if not valid:
            error = 'Authorization failed, unsufficient privilege'
            userinfo = None
    return valid, cookie, error, userinfo


def inner_authenticate(func):
    """ a decorator which filter authenticated user

    it will raise an Unauthorize exception with a json error msg if it
    failed and call the wrapped function if it succeed
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        """ inner wrapped function
        """
        valid, cookie, msg, userinfo = is_authenticated(request)
        if not valid:
            g.cookie = None
            g.userinfo = None
            #print 'Unauthorized: {0}'.format(msg)
            raise Unauthorized(msg)
        else:
            g.cookie = cookie
            g.userinfo = userinfo

        # user is authenticated, thus call func (usually a view)
        return func(*args, **kwargs)
    return wrapper


def inner_allow(access_control):
    """ a decorator which authenticate and then validate access rules

    it will raise an Unauthorize exception with a json error msg if it
    failed and call the wrapped function if it succeed
    """
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            valid, g.cookie, msg, g.userinfo = \
                is_allowed(request, access_control)
            if not valid:
                raise Forbidden(msg)
            return func(*args, **kwargs)
        return wrapper
    return decorator


