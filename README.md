# SSO: a Single Sign On with a rest api

this is a simple SSO for web/mobile application

# Usage

Implement a classical SSO. You can create users and groups and define roles.
Security is done via an encrypted cookie.

Here is a primer for impatient readers and the complete documentation is in the [wiki](http://bitbucket.org/pierreaubert/sso/wiki).

* [wiki: install](http://bitbucket.org/pierreaubert/sso/wiki/Installation)
* [wiki: tests](http://bitbucket.org/pierreaubert/sso/wiki/Tests)
* [wiki: tutorial](http://bitbucket.org/pierreaubert/sso/wiki/Tutorial)

# Primer


## How to install
on a linux or mac os server, the simple way:

```
> mkvirtualvenv sso
> workon sso
> pip -r requirements.txt
> ... wait a bit ...
> edit cfg_dev.conf.py (nothing to do if you want a sqlite database)
> python wsgi.py
```

more details on [wiki: install](http://bitbucket.org/pierreaubert/sso/wiki/Installation)

## How to tests
```
> pip install -r requirements-tests.txt
> py.test --cov sso
```

more details on [wiki: tests](http://bitbucket.org/pierreaubert/sso/wiki/Tests)

## Examples of what you can do

### Start the server
```
> python wsgi.py
```
or start it with a faster server, for example gunicorn
```
> pip install gunicorn
> gunicorn --bind=127.0.0.1:8080 --workers=2 wsgi:app
```

### First tests
```
> curl -X POST http://localhost:8082/login
{ "status": "ko", "error": "401: Unauthorized (No username)" }
```

of course you didn't provide a login/password, let us try again:

```
> curl -c session.txt -X POST -F username=admin -F password=admin2013 http://127.0.0.1:8082/login
{ "status": "ok" }
```
let us see if i'am loggued:
```
> curl -b session.txt http://127.0.0.1:8082/login
{
  "status": "ok"
}%
```
and get my profile:
```
> curl -b session.txt http://127.0.0.1:8082/profile
{
  "status": "ok",
  "users": {
    "creation_date": "Tue, 14 Jan 2014 16:49:58 GMT",
    "email": "admin@localhost",
    "error_login": 0,
    "last_login_date": "Tue, 14 Jan 2014 16:49:58 GMT",
    "user_id": 1,
    "username": "admin"
  }
}%
```

more details on [wiki: tutorial](http://bitbucket.org/pierreaubert/sso/wiki/Tutorial)

# And now

Full documentation is in the [sso wiki](http://bitbucket.org/pierreaubert/sso/wiki).

Thanks for reading this far.
