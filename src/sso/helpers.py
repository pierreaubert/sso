#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

from werkzeug.exceptions import HTTPException
from flask import g, current_app, jsonify, request


def json_status(error, set_cookie=False, cookie=None):
    """ return a json msg to tell ok or ko
    if ko, error is appended
    set the cookie if not None
    """
    if error is None:
        resp = {
            'status': 'ok'
        }
    else:
        resp = {
            'status': 'ko',
            'error': error
        }

    response = jsonify(resp)
    if set_cookie:
        if cookie is None:
            # delete cookie
            response.set_cookie('ssosign', value='', expires=0)
        else:
            # set cookie
            response.set_cookie('ssosign',
                                value=cookie,
                                max_age=current_app.cm.get_seconds(cookie),
                                path=current_app.cm.path)
    return response


class JsonHTTPException(HTTPException):
    """
    create a standard HTTPException but return a JSON compatible answer
    """
    def __init__(self, description=None, response=None):
        """
        initalize exception with an error message

        :param description error message
        :param response a wsgi response
        """
        HTTPException.__init__(self)
        if description is not None:
            self.description = description
        self.response = response

    def get_description(self, environ=None):
        """ get the description

        :param environ wsgi environ
        :return inner description in text
        """
        return self.description

    def get_body(self, environ=None):
        """ return body

        :param environ wsgi environ
        :return standard return in json
        """
        return '{{ "status": "ko", "error": "{0}" }}'.format(
            self.get_description(environ))

    def get_headers(self, environ=None):
        """ return headers

        :param environ wsgi environ
        :return a list of http headers
        """
        return [('Content-Type', 'text/json')]


def get_caller_ip(request):
    """ get the IP from the caller (without IP from reverse proxy)
    """
    if len(request.access_route) > 0:
        caller_ip = request.access_route[0]
    else:
        # humm TODO
        caller_ip = '0.0.0.0'
    return caller_ip


def sso_db_get_conn():
    """ get a connection from pool and store it in global g
    """
    name = 'sso_conn'
    conn = getattr(g, name, None)
    if conn is None:
    #    print '(trace) > create conn ({0})'.format(name)
        conn = g.sso_conn = current_app.sso_db.connect()
    #else:
    #    print '(trace) > reuse conn ({0})'.format(name)
    return conn


def db_close_conn(exception):
    name = 'sso_conn'
    conn = getattr(g, name, None)
    if conn is not None:
    #    print '(trace) > close conn ({0})'.format(name)
        conn.close()
    #else:
    #    print '(trace) > already closed conn ({0})'.format(name)


def one2dict(stmt, cursor):
    """ return a dict with (column name,value) pairs
    match the return of fetchone()
    """
    d = {}
    for col in stmt.columns:
        d[col.name] = getattr(cursor, col.name)
    return d


def row2dict(stmt, row):
    """ transform 1 row returned by DB dict """
    d = {}
    for col, val in zip(stmt.columns, row):
        d[col.name] = val
    return d


def rows2dict(stmt, rows):
    """ transform the rows returned by DB into a list of dicts
    match the return of fetchall()
    """
    return [row2dict(stmt, row) for row in rows]


def db_schema_exist(engine, tables):
    """ return true is database exist and
    schema has been uploaded

    deprecated use instead:
             table.create(engine, checkfirst=True)
    """
    import sqlalchemy as sa
    for table in tables:
        s = sa.select([table]).limit(1)
        try:
            conn = engine.connect()
            conn.execute(s).fetchone()
            # print 'table ' + table + ' exist'
        except sa.exc.NoSuchTableError:
            return False
        except sa.exc.OperationalError:
            return False
        except sa.exc.SQLAlchemyError:
            return False
    return True


def check_restfull():
    """ check that request is JSON content and associated header
    if encoding is NOT utf-8, change here
    """
    # check encoding
    # check headers accept/json
    # ...
    return True


def check_parameters():
    """ obsolete """
    return True


def get_limit(request):
    """ return limit (number of row to return from query)
    """
    limit = 10
    if request.args.get('limit'):
        limit = request.args.get('limit')
        # limit = min(1000, int(limit))
    return limit


def get_offset(request):
    """ return limit (number of row to return from query)
    """
    offset = 0
    if request.args.get('offset'):
        offset = request.args.get('offset')
    return offset


def set_userid():
    """ set user_id if any in g
    """
    user_id = getattr(g, 'user_id', None)
    if user_id is None:
        if 'ssosign' in request.cookies:
            cookie = request.cookies['ssosign']
            user_id = g.user_id = current_app.sso_client.get_userid(cookie)

    if user_id is None:
        error = 'not authenticated'
        return json_status(error)
