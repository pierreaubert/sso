#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
import unittest
import logging
import json

import httpretty
from sso.client.rest import Client
import sso.accesscontrol.exceptions as ac


sso_url = 'http://sso.7pi.eu'
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('/tmp/{0}.log'.format(__name__))
logger.addHandler(fh)


class SSORestClientTests(unittest.TestCase):
    """ test sso rest client

    all answers from sso server are mocked via httpretty
    """

    def setUp(self):
        self.client = Client(sso_url, logger)

    def tearDown(self):
        pass

    @httpretty.activate
    def test_client_post_ok(self):
        """ mock a ok
        """
        httpretty.register_uri(
            httpretty.POST, sso_url + '/login',
            body='{ "status": "ok" }',
            status=200)
        try:
            self.client.post_login('username', 'password')
        except ac.Unauthorized:
            assert False
        except ac.Forbidden:
            assert False
        except ac.BadRequest:
            assert False
        except ac.InternalServerError:
            assert False

    @httpretty.activate
    def test_client_post_ko0(self):
        """ mock a OK but with a wrong body
        """
        httpretty.register_uri(
            httpretty.POST, sso_url + '/login',
            body='grrr',
            status=200)
        with self.assertRaises(ac.InternalServerError) as e:
            self.client.post_login('username', 'password')
            assert 'json' in e.exception.get_description()

    @httpretty.activate
    def test_client_post_ko1(self):
        """ mock a KO thus it must throw
        """
        httpretty.register_uri(
            httpretty.POST, sso_url + '/login',
            body='{"status": "ko"}',
            status=401)
        with self.assertRaises(ac.Unauthorized) as e:
            self.client.post_login('username', 'password')
            assert 'Unauthorized' in e.exception.get_description()

    @httpretty.activate
    def test_client_post_ko2(self):
        """ mock a KO thus it must throw
        """
        the_msg = 'check msg'
        the_body = {
            'status': 'ko',
            'error': the_msg,
        }
        httpretty.register_uri(
            httpretty.POST,
            sso_url + '/login',
            body=json.dumps(the_body),
            status=401)
        with self.assertRaises(ac.Unauthorized) as e:
            self.client.post_login('username', 'password')
        assert the_msg in e.exception.get_description()

    @httpretty.activate
    def test_client_get_ok(self):
        """ mock a ok
        """
        httpretty.register_uri(
            httpretty.GET, sso_url + '/login',
            body='{ "status": "ok" }',
            status=200)
        self.client.get_login()

    @httpretty.activate
    def test_client_get_ko1(self):
        """ mock a ko: bad cookie
        """
        httpretty.register_uri(
            httpretty.GET, sso_url + '/login',
            body='{ "status": "ko", "error": "mocked" }',
            status=401)
        with self.assertRaises(ac.Unauthorized) as e:
            self.client.get_login()
        assert 'mocked' in e.exception.get_description()

    @httpretty.activate
    def test_client_profile_ok(self):
        """ mock a ok
        """
        the_body = {
            'status': 'ok',
            'users': {
                'email': 'support@7pi.eu'
            },
        }
        httpretty.register_uri(
            httpretty.GET, sso_url + '/profile',
            body=json.dumps(the_body),
            status=200)
        profile = self.client.get_profile()
        assert 'email' in profile
        assert profile['email'] in 'support@7pi.eu'

    @httpretty.activate
    def test_client_getuserid_ok(self):
        """ mock a ok
        """
        the_body = {
            'status': 'ok',
            'users': {
                'user_id': '2014',
                'username': 'pierre'
            },
        }
        httpretty.register_uri(
            httpretty.GET, sso_url + '/profile',
            body=json.dumps(the_body),
            status=200)
        user_id = self.client.get_userid()
        assert user_id in '2014'

    @httpretty.activate
    def test_client_getusername_ok(self):
        """ mock a ok
        """
        the_body = {
            'status': 'ok',
            'users': {
                'user_id': '2014',
                'username': 'pierre'
            },
        }
        httpretty.register_uri(
            httpretty.GET, sso_url + '/profile',
            body=json.dumps(the_body),
            status=200)
        username = self.client.get_username()
        assert username in 'pierre'


if __name__ == '__main__':
    unittest.main()
