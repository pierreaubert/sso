#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

import logging
from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError

# get utils
from helpers import db_close_conn

# get models
from models import sso_db, Users, Groups

# get api
from apis import LoginAPI, FollowersAPI, ProfileAPI, \
    GroupsAPI, UsersGroupsAPI, AccessControlAPI

# get cm
from cookiemanager import CookieManager


def db_check(engine):
    """ create the databases associated with ssi
    """
    sso_db.create_all(engine, checkfirst=True)

    #from alembic.config import Config
    #from alembic import command
    #alembic_cfg = Config("alembic.ini")
    #command.stamp(alembic_cfg, "head")


def create_admin_user(app):
    """ create default admin user if it doesn't exist """
    db_conn = app.sso_db.connect()
    db_users = Users(db_conn)
    db_groups = Groups(db_conn)
    try:
        # create groups if they do not exists
        group_admin_id = db_groups.id('admin')
        if group_admin_id is None:
            group_admin_id = db_groups.create('admin')
        if group_admin_id is None or 0:
            app.logger.error('Cannot create group admin!')
            return None

        if db_groups.id('users') is None:
            db_groups.create('users')
        if db_groups.id('anonymous') is None:
            db_groups.create('anonymous')
        # create admin user if it does not exist
        if db_users.id(app.config['ADMIN_USERNAME']) is None:
            db_users._create(app.config['ADMIN_USERNAME'],
                             app.config['ADMIN_PASSWORD'],
                             app.config['ADMIN_EMAIL'],
                             group_admin_id,
                             app.logger)
    except IntegrityError:
        app.logger.error('Catch an integrity error')


def create_sso():
    """ setup the sso application
    """
    app = Flask('sso')
    #env = sso_config.Config()
    #sso.config.from_object(env)
    app.config.from_envvar('SSO_SETTINGS')
    #print 'sso version: ' + app.config['VERSION']
    #print 'sso version: ' + app.config['DATABASE_URI']

    # add a file logger
    app.logger.setLevel(app.config['LOG_LEVEL'])
    fh = logging.FileHandler(app.config['LOG_FILE'])
    fm = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    fh.setFormatter(fm)
    app.logger.addHandler(fh)

    # if using Sentry add a logger
    if len(app.config['SENTRY_DSN']) > 0:
        # if you want to use Sentry
        from raven.contrib.flask import Sentry
        from raven.handlers.logging import SentryHandler

        sentry = Sentry(app, dsn=app.config['SENTRY_DSN'])
        sentry.init_app(app)
        sh = SentryHandler(app.config['SENTRY_DSN'])
        app.logger.addHandler(sh)

    # fire engine
    app.sso_db = create_engine(app.config['DATABASE_URI'])
    db_check(app.sso_db)

    # add login routes
    app.add_url_rule('/login', view_func=LoginAPI.as_view('login'),
                     methods=['GET', 'POST', 'DELETE'])
    # add group routes
    app.add_url_rule('/groups',
                     view_func=GroupsAPI.as_view('groups0'),
                     methods=['GET'])
    app.add_url_rule('/groups/<groupname>',
                     view_func=GroupsAPI.as_view('groups1'),
                     methods=['GET', 'POST'])
    app.add_url_rule('/groups/<groupname>/parent/<parent_groupname>',
                     view_func=GroupsAPI.as_view('groups2'),
                     methods=['PUT', 'POST'])
    # add profile rules
    app.add_url_rule('/profile', view_func=ProfileAPI.as_view('profile'),
                     methods=['GET', 'POST'])
    app.add_url_rule('/profile/<username>',
                     view_func=ProfileAPI.as_view('profile1'),
                     methods=['DELETE'])
    app.add_url_rule('/profile/<username>/groups',
                     view_func=UsersGroupsAPI.as_view('profilegroups'),
                     methods=['GET'])
    app.add_url_rule('/profile/<username>/group/<groupname>',
                     view_func=UsersGroupsAPI.as_view('profilegroups2'),
                     methods=['POST', 'PUT', 'DELETE'])
    # add followers rules
    app.add_url_rule('/followers/<followname>',
                     view_func=FollowersAPI.as_view('followers'),
                     methods=['POST'])
    app.add_url_rule('/followers/in',
                     view_func=FollowersAPI.as_view('followers/in'),
                     methods=['GET'])
    app.add_url_rule('/followers/out',
                     view_func=FollowersAPI.as_view('followers/out'),
                     methods=['GET'])
    # add accesscontrol rules
    app.add_url_rule('/accesscontrol',
                     view_func=AccessControlAPI.as_view('accesscontrol'),
                     methods=['GET','POST'])

    # add error handlers
    # use a decorator below as advised in doc
    # app.error_handler_spec[None][401] = sso_401

    # BUG should check too simple 'secret'
    secret = app.config['SECRET_KEY']
    # build an encrypter for cookie
    app.cm = CookieManager(secret)

    # add @app.teardown_appcontext
    app.teardown_appcontext(db_close_conn)

    # create default group and users
    create_admin_user(app)

    return app


