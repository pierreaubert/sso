#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
import unittest

# # python 2.7.2
# class Cache1(type):
#     __metaclass__ = MetaCache
#     def __init__(self, name):
#         print 'calling cache1('+name+')'
#         self.name = name
#
# #class Cache1(metaclass=MetaCache):
# #    def __init__(self, name):
# #        self.name = name
#
# class MetaClassTests(unittest.TestCase):
#
#     def setUp(self):
#         pass
#
#     def tearDown(self):
#         pass
#
#     def test_c1_check(self):
#         a = Cache1('a')
#         b = Cache1('b')
#         c = Cache1('a')
#         assert a is c
#         assert b is not a


if __name__ == '__main__':
    unittest.main()
