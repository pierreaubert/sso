#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
import os
import unittest
from exceptions import IOError
from sqlalchemy.exc import IntegrityError

from sso.accesscontrol.ac import AC_IP, AC_Or, AC_And, AC_Not, \
    AC_Group, AC_UserGroup, AC_User, AccessConstraint
from sso.accesscontrol.parser import AC_Parser
from sso.models import Users, Groups, UsersGroups
from sso.appsso import create_sso


class AccessControlTests(unittest.TestCase):

    def setUp(self):
        # setup db
        self.server = create_sso()
        self.logger = self.server.logger
        self.server.testing = True
        # setup some test data
        self.test_user = {
            'username': 'utest1',
            'groupname': 'gtest1',
            'ip': '1.1.1.1'
        }
        self.test_user2 = {
            'username': 'utest2',
            'groupname': 'gtest2',
            'ip': '2.2.2.2'
        }
        self.test_user3 = {
            'username': 'utest3',
            'groupname': 'gtest3',
            'ip': '3.3.3.3'
        }
        self.test_admin = {
            'username': 'admin',
            'groupname': 'admin',
            'ip': '0.0.0.0'
        }
        self.populate_groups()
        self.populate_users()

    def tearDown(self):
        try:
            fname = self.server.config['DATABASE_URI']
            pos = fname.find('://')
            name = fname[pos+3:]
            if os.access(name, os.F_OK):
                os.unlink(name)
                pass
            else:
                print '''Can't open/close db file!'''
        except IOError:
            print '''Can't open/close db file!'''

    def populate_groups(self):
        """ lazy way """
        db_conn = self.server.sso_db.connect()
        # -------------------- GROUPS --------------------
        db_groups = Groups(db_conn)
        # create group gtest1
        try:
            db_groups.create('gtest1')
        except IntegrityError:
            pass
        # create group gtest2 (child of gtest1)
        try:
            id1 = db_groups.id('gtest1')
            db_groups.create('gtest2', id1)
        except IntegrityError:
            pass
        # create group gtest3 (child of gtest2)
        try:
            id2 = db_groups.id('gtest2')
            db_groups.create('gtest3', id2)
        except IntegrityError:
            pass
        # create group gtest4 (child of gtest2)
        try:
            id2 = db_groups.id('gtest2')
            db_groups.create('gtest4', id2)
        except IntegrityError:
            pass

    def populate_users(self):
        """ lazy way """
        db_conn = self.server.sso_db.connect()
        # -------------------- USERS --------------------
        db_groups = Groups(db_conn)
        db_users = Users(db_conn)
        # create user utest1 in group tgroup1
        try:
            id1 = db_groups.id('gtest1')
            db_users._create('utest1', 'pwd1', 'u1@7pi.eu', id1, 
                             self.logger)
        except IntegrityError:
            pass
        # create user utest2 in group tgroup2
        try:
            id2 = db_groups.id('gtest2')
            db_users._create('utest2', 'pwd2', 'u2@7pi.eu', id2,
                             self.logger)
        except IntegrityError:
            pass
        # create user utest3 in group tgroup3
        try:
            id3 = db_groups.id('gtest3')
            db_users._create('utest3', 'pwd3', 'u3@7pi.eu', id3,
                             self.logger)
        except IntegrityError:
            pass

    def populate_usersgroups(self):
        """ lazy way """
        db_conn = self.server.sso_db.connect()
        # -------------------- USERGROUPS ---------------
        db_usersgroups = UsersGroups(db_conn)
        # add group admin to utest1
        try:
            db_usersgroups.create('utest1', 'admin')
        except IntegrityError:
            pass

    def test_default(self):
        """ validate default
        """
        t = AccessConstraint()
        assert t.is_allowed(self.test_user) is False

    def test_ac_user(self):
        """ check same user
        """
        t = AC_User(self.test_user['username'])
        assert t.is_allowed(self.test_user) is True
        assert t.is_allowed(self.test_user2) is False

    def test_ac_ip(self):
        """ check same ip
        """
        t = AC_IP(self.test_user['ip'])
        assert t.is_allowed(self.test_user) is True
        assert t.is_allowed(self.test_user2) is False

    def test_ac_group(self):
        """ check same group
        or check user's group is in group hierarchy
        """
        with self.server.app_context():
            t1 = AC_Group('gtest1')
            assert t1.is_allowed(self.test_user) is True
            assert t1.is_allowed(self.test_user2) is True
            assert t1.is_allowed(self.test_user3) is True
            t2 = AC_Group('gtest2')
            assert t2.is_allowed(self.test_user) is False
            assert t2.is_allowed(self.test_user2) is True
            assert t2.is_allowed(self.test_user3) is True
            t3 = AC_Group('gtest3')
            assert t3.is_allowed(self.test_user) is False
            assert t3.is_allowed(self.test_user2) is False
            assert t3.is_allowed(self.test_user3) is True

    def test_ac_usergroup(self):
        """ check a user's groups is matching the group
        """
        with self.server.app_context():
            # add relations
            self.populate_usersgroups()
            # check them
            t = AC_UserGroup('gtest1')
            assert t.is_allowed(self.test_user) is True
            assert t.is_allowed(self.test_user2) is True
            assert t.is_allowed(self.test_user3) is True
            t2 = AC_UserGroup('gtest2')
            assert t2.is_allowed(self.test_user) is False
            assert t2.is_allowed(self.test_user2) is True
            assert t2.is_allowed(self.test_user3) is True
            t3 = AC_UserGroup('gtest3')
            assert t3.is_allowed(self.test_user) is False
            assert t3.is_allowed(self.test_user2) is False
            assert t3.is_allowed(self.test_user3) is True
            ta = AC_UserGroup('admin')
            assert ta.is_allowed(self.test_user) is True
            assert ta.is_allowed(self.test_user2) is False
            assert ta.is_allowed(self.test_user3) is False

    def test_ac_and(self):
        """ check a user's groups is matching the group
        """
        with self.server.app_context():
            tu1 = AC_User('admin')
            tu2 = AC_User('utest1')
            ta = AC_UserGroup('admin')
            t1 = AC_And(tu1, ta)
            t2 = AC_And(tu2, ta)
            assert t1.is_allowed(self.test_admin) is True
            assert t1.is_allowed(self.test_user) is False
            # add relations
            self.populate_usersgroups()
            assert t2.is_allowed(self.test_admin) is False
            assert t2.is_allowed(self.test_user) is True

    def test_ac_or(self):
        """ check a user's groups is matching the group
        """
        with self.server.app_context():
            tu1 = AC_User('admin')
            tu2 = AC_User('utest1')
            ta = AC_UserGroup('admin')
            t1 = AC_Or(tu1, ta)
            t2 = AC_Or(tu2, ta)
            assert t1.is_allowed(self.test_admin) is True
            assert t1.is_allowed(self.test_user) is False
            assert t2.is_allowed(self.test_admin) is True
            assert t2.is_allowed(self.test_user) is True

    def test_ac_and_or(self):
        """ check a user's groups is matching the group
        """
        with self.server.app_context():
            tu1 = AC_User('admin')
            tu2 = AC_User('utest1')
            ta = AC_UserGroup('admin')
            t = AC_And(AC_Or(tu1, tu2), ta)
            print t
            assert t.is_allowed(self.test_admin) is True
            assert t.is_allowed(self.test_user) is False

    def test_ac_not(self):
        """ check a user's groups is matching the group
        """
        t = AC_Not(AccessConstraint())
        assert t.is_allowed(self.test_user) is True


    def _check_rule(self, rule1):
        text1 = str(rule1)
        parser = AC_Parser()
        rule2 = parser.parse(text1)
        text2 = str(rule2)
        self.assertEqual(text1, text2)

    def test_ac_parser(self):
        with self.server.app_context():
            r1 = AC_IP('10.0.0.1')
            r2 = AC_User('admin')
            r3 = AC_UserGroup('admin')
            self._check_rule(r1)
            self._check_rule(r2)
            self._check_rule(r3)
            self._check_rule(AC_And(r1, r2))
            self._check_rule(AC_Or(r1, r3))
            self._check_rule(AC_Not(r3))
            self._check_rule(AC_And(AC_Or(r1,r3), AC_Or(r2,r3)))
        

if __name__ == '__main__':
    unittest.main()
