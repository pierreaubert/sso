#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

import time
import binascii
from Crypto.Cipher import Blowfish


class CookieManager:
    """ manage our sso cookie
    """

    def __init__(self,
                 secret,
                 short_seconds=15*60,
                 long_seconds=86400*30,
                 name='sso',
                 path='/',
                 max_counter=1024):
        """
        secret (the secret key for the encoding algo)

        the cookie can be a short or a long cookie

        short_seconds (how many seconds the cookie is valid)
        long_seconds (how many seconds the cookie is valid)
        name of the cookie
        path where the cookie apply
        max_counter cookie is invalid after max_counter update
        """
        assert secret is not None
        assert len(secret) > 16
        self.coder = Blowfish.new(secret)
        self.short_seconds = short_seconds
        self.long_seconds = long_seconds
        self.name = name
        self.path = path
        self.max_counter = max_counter
        # the char used to split on
        # infos encoded into the cookie can't contain this character
        self.char_split = '~'
        # the char used to fill the cookie
        # infos encoded into the cookie can't contain this character
        self.char_filler = '$'
        self.chars_end = self.char_split+self.char_filler

    def pad8(self, text):
        """ pad to 8 ascii characters
        """
        fill = '$$$$$$$$'
        lround = len(text) % 8
        if lround == 0:
            padded = text
        else:
            lround = 8-lround
            padded = text+fill[:lround]
        return padded

    def unpad8(self, text):
        """ unpad """
        pos = text.rfind(self.chars_end)
        return text[:pos+2]

    def encode(self, cookie):
        """ encode cookie """
        encoded = self.coder.encrypt(self.pad8(cookie))
        ascii = binascii.b2a_hex(encoded)
        return ascii

    def decode(self, cookie):
        """ decode cookie
        """
        binary = binascii.a2b_hex(cookie)
        decoded = self.coder.decrypt(binary)
        data = self.unpad8(decoded)
        return data

    def create(self, username, ip, short, counter=0):
        """ create a cookie from username, ip and time

        if short is True, make it valid for self.short_seconds else for
        self.long_seconds
        """
        short_or_long = 's'
        if not short:
            short_or_long = 'l'
        # build the cookie
        # TODO: must throw if ~ in username or ip
        cook = '{username}{s}{ip}{s}{sol}{s}{now}{s}{counter}{s}{filler}'\
            .format(
                username=username.replace(self.char_split, ''),
                s=self.char_split,
                ip=ip.replace(self.char_split, ''),
                sol=short_or_long,
                now=time.time(),
                counter=str(int(counter)+1),
                filler=self.char_filler)
        # encrypt it
        return self.encode(cook)

    def split(self, cookie):
            """ split on self.char_split char
            [0] is username
            [1] is ip
            [2] is short or long cookie ('s'/'l')
            [3] is generated time for the cookie
            [4] is current counter
            """
            #print 'cookie:'+cookie
            return cookie.split(self.char_split)

    def is_valid_verbose(self, cook, ip):
        """ check the validity of the authentification
        if cookie is valid return
             True, updated cookie, 'ok', dict (with the data from the cookie)
        else return
             False, None, error message, None
        """
        #print '(trace) > ' + cook
        if cook is None:
            return False, None, 'not a cookie', None
        # decode
        decoded_cook = self.decode(cook)
        if decoded_cook is None:
            return False, None, 'decode failed', None
        # split
        c_username = c_ip = c_sol = c_time = c_counter = c_pad = None
        try:
            c_username, c_ip, c_sol, c_time, c_counter, c_pad = \
                self.split(decoded_cook)
        except ValueError:
            return False, None, 'malformed', None
        # check time
        now = time.time()
        c_time = float(c_time)
        valid = False
        # be carefull of precision here (add 1 sec to be sure)
        if (now+1.0) >= c_time:
            delta = now-c_time
            if c_sol == 's':
                short = True
                valid = delta < self.short_seconds
            else:
                short = False
                valid = delta < self.long_seconds
            if not valid:
                return False, None, 'outdated', None
        else:
            return False, None, 'time error  ({0},{1})'\
                .format(now, c_time), None

        # check ip
        if c_ip != ip:
            return False, None, 'ip check failed', None

        # check counter, it should not go too high (beast attack for ex)
        if int(c_counter) < 0 or int(c_counter) > self.max_counter:
            return False, None, 'too many access', None

        # pack answers
        datas = {
            'username': c_username,
            'ip': c_ip,
            'sol': c_sol,
            'counter': c_counter,
            'sincecreation': now-c_time
        }
        return True, self.create(c_username, c_ip, short, c_counter), \
            'ok', datas

    def is_valid(self, cook, ip):
        """ check the validity of the authentification
        return (True,cookie) if cookie is valid and the updated cookie
        return (False, None) if it is not valid
        """
        valid, cook, msg, datas = self.is_valid_verbose(cook, ip)
        return valid, cook, msg

    def extract_username(self, decoded_cookie):
        """ extract username from a valid decoded cookie """
        datas = self.split(decoded_cookie)
        return datas[0]

    def extract_ip(self, decoded_cookie):
        """ extract ip from a valid decoded cookie """
        datas = self.split(decoded_cookie)
        return datas[1]

    def extract_seconds(self, decoded_cookie):
        """ extract ip from a valid decoded cookie """
        datas = self.split(decoded_cookie)
        if datas[2] == 's':
            return self.short_seconds
        else:
            return self.long_seconds

    def extract_counter(self, decoded_cookie):
        """ extract ip from a valid decoded cookie """
        datas = self.split(decoded_cookie)
        return datas[4]

    def get_username(self, cook):
        """ return username from a cookie
        """
        decoded_cookie = self.decode(cook)
        if decoded_cookie is None:
            return None
        return self.extract_username(decoded_cookie)

    def get_seconds(self, cook):
        """ return the number of second for a cookie
        return 0 if invalid
        """
        decoded_cookie = self.decode(cook)
        if decoded_cookie is None:
            return 0
        return self.extract_seconds(decoded_cookie)

    def get_ip(self, cook):
        """ return the ip for a cookie
        return None if invalid
        """
        decoded_cookie = self.decode(cook)
        if decoded_cookie is None:
            return None
        return self.extract_ip(decoded_cookie)

    def get_counter(self, cook):
        """ return the current counter for a cookie
        return None if invalid
        """
        decoded_cookie = self.decode(cook)
        if decoded_cookie is None:
            return None
        return self.extract_counter(decoded_cookie)
