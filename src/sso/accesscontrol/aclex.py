#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
import ply.lex

class AC_Lex:

    # reserved list of words
    _reserved = {
        'ip': 'IP',
        'username': 'USERNAME',
        'usergroups': 'USERGROUPS',
        'groupname': 'GROUPNAME',
        'and': 'AND',
        'or': 'OR',
        'not': 'NOT',
        'set': 'SET',
    }
    
    # list of tokens
    tokens = [ 'LPAREN', 'RPAREN', 'SQUOTE', 'NAME', 'COMMA', 'IPV4', 
               'LBRAC', 'RBRAC'] + list(_reserved.values())
    
    t_ignore = ' '
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_LBRAC = r'\['
    t_RBRAC = r'\]'
    t_SQUOTE = r'\''
    t_COMMA = r','
    # validating ip v4
    t_IPV4 = r'(2[0-4][0-9]|[01]?[0-9]?[0-9]|25[0-5])\D{1,5}(2[0-4][0-9]|[01]?[0-9]?[0-9]|25[0-5])\D{1,5}(2[0-4][0-9]|[01]?[0-9]?[0-9]|25[0-5])\D{1,5}(2[0-4][0-9]|[01]?[0-9]?[0-9]|25[0-5])'
    
    # cheat the lexer and return an ID if name match a reserved word
    def t_NAME(self, t):
        r'[a-z][a-z0-9]*'
        t.type = self._reserved.get(t.value,'NAME')
        return t

    def lexer(self):
        return ply.lex.lex(module=self)
        # return ply.lex.lex(optimize=1, module=self)

