#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

from datetime import datetime
from werkzeug import check_password_hash, generate_password_hash
from sqlalchemy import Table, Column, Integer, String, UniqueConstraint, \
    MetaData, Sequence, Index, DateTime, ForeignKey, \
    select, delete
from sqlalchemy.exc import IntegrityError
from helpers import one2dict, rows2dict
from flask import current_app
from sso.accesscontrol.exceptions import BadRequest

sso_db = MetaData()

# groups of user (think 'admin' or 'user')
groups = Table(
    'groups',
    sso_db,
    Column('group_id', Integer, Sequence('groups_seq'), primary_key=True),
    Column('groupname', String(16), nullable=False),
    Column('parent_id', Integer, nullable=True),
    UniqueConstraint('groupname', name='group_groupname_uniq'))

Index('groups_groupname_idx', groups.c.groupname)

# table of users (which belongs to a group)
users = Table(
    'users',
    sso_db,
    Column('user_id', Integer, Sequence('users_seq'), primary_key=True),
    Column('username', String(16), nullable=False),
    Column('email', String(60), nullable=False),
    Column('password_hash', String(128), nullable=False),
    Column('creation_date', DateTime),
    Column('last_login_date', DateTime),
    Column('error_login', Integer),
    UniqueConstraint('username', name='users_username_uniq'),
    UniqueConstraint('email', name='users_email_uniq'))

Index('users_username_idx', users.c.username)
Index('users_email_idx', users.c.email)

# models the groups a user belongs to
usersgroups = Table(
    'usersgroups',
    sso_db,
    Column('usergroup_id', Integer, Sequence('usergroup_seq'),
           primary_key=True),
    Column('user_id', Integer, ForeignKey('users.user_id'), nullable=False),
    Column('group_id', Integer, ForeignKey('groups.group_id'), nullable=False),
    UniqueConstraint('user_id', 'group_id', name='usersgroups_usergroup_uniq'))

Index('usersgroups_user_idx', usersgroups.c.user_id)

# table of who is folling whom
followers = Table(
    'followers',
    sso_db,
    Column('who_id', Integer, ForeignKey('users.user_id'), nullable=False),
    Column('whom_id', Integer, ForeignKey('users.user_id'), nullable=False),
    UniqueConstraint('who_id', 'whom_id', name='followers_uniq'))

Index('followers_who_idx', followers.c.who_id)
Index('followers_whom_idx', followers.c.whom_id)


class Groups():

    def __init__(self, conn):
        self.conn = conn
        self.lastrowid = -1

    def id(self, groupname):
        """ return rowid or None for a given groupname
        """
        s = select([groups]).where(groups.c.groupname == groupname)
        group = self.conn.execute(s).fetchone()
        if group is None:
            return None
        else:
            return group['group_id']

    def create(self, groupname, parent_id=None):
        """ add a new group with a parent
        """
        # print 'Create groupname: ' + groupname
        if parent_id is None:
            ins = groups.insert().values(groupname=groupname)
        else:
            # print 'setting parent_id to '+str(parent_id)
            ins = groups.insert().values(groupname=groupname,
                                         parent_id=parent_id)
        
        self.lastrowid = -1
        try:
            group = self.conn.execute(ins)
            self.lastrowid = group.inserted_primary_key[0]
        except IntegrityError:
            # group already exist
            # print 'Get a integriry error for groupname: ' + groupname
            pass
        finally:
            return self.lastrowid

    def read(self, groupname=None):
        """ return 0 or 1 row
        """
        if groupname is not None:
            s = select([groups]).where(groups.c.groupname == groupname)
            group = self.conn.execute(s).fetchone()
            if group is not None:
                return one2dict(s, group)
        else:
            s = select([groups])
            group = self.conn.execute(s).fetchall()
            if group is not None:
                return rows2dict(s, group)
        return None

    def update(self, groupname, parent_id):
        """ change parent_id for a group
        """
        upd = groups.update()\
            .where(groups.c.groupname == groupname)\
            .values(parent_id=parent_id)
        group = self.conn.execute(upd)
        # do not work for all engine
        self.lastrowid = group.lastrowid

    def delete(self, id):
        """ Dangerours
        TODO: do we add a on delete cascade to FK definition?
        """
        d = delete([groups]).where(groups.c.group_id == id)
        self.conn.execute(d)

    def _parent(self, groupname):
        """ return the parent of groupname and None if it is null
        """
        g1 = groups.alias()
        g2 = groups.alias()
        s = select([g2.c.groupname]).where(g2.c.group_id == g1.c.parent_id)\
                                    .where(g1.c.groupname == groupname)
        parent = self.conn.execute(s).fetchone()
        if parent:
            # print 'parent of ' + groupname + ' is ' + parent[0]
            return parent[0]
        else:
            return None

    def parents(self, groupname):
        """ return a list of groups with groupname if exist and all its
        parents if any
        """
        l = [groupname]
        p = self._parent(groupname)
        while p is not None:
            l.append(p)
            p = self._parent(p)
        # print 'parents of ' + groupname
        # print l
        return l

    def _child(self, groupname):
        """ return the child of groupname and None if it is null
        """
        g1 = groups.alias()
        g2 = groups.alias()
        s = select([g1.c.groupname]).where(g1.c.parent_id == g2.c.group_id)\
                                    .where(g2.c.groupname == groupname)
        return self.conn.execute(s).fetchall()

    def children(self, groupname):
        """ return a list of groups with groupname if exist and all its
        children if any
        """
        l = [groupname]
        p = self._child(groupname)
        if p:
            for i in p:
                group = i[0]
                l += self.children(group)
        #print 'children of ' + groupname
        #print l
        return l


class Users():

    def __init__(self, conn):
        self.conn = conn
        self.lastrowid = -1

    def _create(self, username, passwd, email, group_id, logger):
        """ add a user
        """
        now = datetime.utcnow()
        # create the hash
        hash = generate_password_hash(passwd, 'sha1', 16)
        #print '> ' + passwd
        #print '> ' + hash
        # insert stmt
        # with self.conn.begin() as trans:
        ins1 = users.insert().values(
            username=username,
            email=email,
            password_hash=hash,
            creation_date=now,
            last_login_date=now,
            error_login=0)
        self.lastrowid = -1
        try:
            user = self.conn.execute(ins1)
            self.lastrowid = user.inserted_primary_key[0]
            if self.lastrowid == 0:
                msg = 'DB return 0 for user_id({0})'.format(username)
                logger.error(msg)
                raise IntegrityError(msg)
            # insert group for user
            ins2 = usersgroups.insert().values(
                user_id=self.lastrowid,
                group_id=group_id)
            self.conn.execute(ins2)
        except IntegrityError as ie:
            if username is not 'admin':
                logger.warn(ie)
        finally:
            return self.lastrowid

    def create(self, username, passwd, email, group_id):
        """ add a user
        this call required the app called to be placed inside a flask app context
        _create is use before the app is created to initialize default accounts
        """
        return self._create(username, passwd, email, group_id,
                            current_app.logger)

    def read(self, username):
        """ return row or None for a given username
        """
        s = select([users]).where(users.c.username == username)
        user = self.conn.execute(s).fetchone()
        if user is None:
            return None
        else:
            return one2dict(s, user)

    def read_public(self, username):
        """ return row or None for a given username and remove pw_hash
        """
        d = self.read(username)
        if d is None:
            return d
        d2 = {}
        for i in d:
            if i != 'password_hash':
                d2[i] = d[i]
        return d2

    def delete(self, id):
        """ TODO """
        d = delete([users]).where(users.c.user_id == id)
        self.conn.execute(d)

    def is_password_valid(self, hash, passwd):
        """ return true if passw matches
        """
        #print '< ' + passwd
        #print '< ' + hash
        return check_password_hash(hash, passwd)

    def id(self, username):
        """ return rowid or None for a given username
        """
        s = select([users]).where(users.c.username == username)
        user = self.conn.execute(s).fetchone()
        if user is None:
            return None
        else:
            return user['user_id']

    def email(self, email):
        """ return rowid or None for a given username
        """
        s = select([users.c.email]).where(users.c.email == email)
        user = self.conn.execute(s).fetchone()
        if user is None:
            return None
        else:
            return user['email']


class UsersGroups():

    def __init__(self, conn):
        self.conn = conn
        self.lastrowid = -1

    def create(self, username, groupname):
        """ add group in user's group list
        """
        sel_user = select([users.c.user_id])\
            .where(users.c.username == username)
        uid = self.conn.execute(sel_user).fetchone()
        if uid is None:
            raise BadRequest('username {0} unkown'.format(username))
        sel_group = select([groups.c.group_id]).where(
            groups.c.groupname == groupname)
        gid = self.conn.execute(sel_group).fetchone()
        if gid is None:
            raise BadRequest('groupname {0} unkown'.format(groupname))
        ins = usersgroups.insert().values(user_id=uid['user_id'], 
                                          group_id=gid['group_id'])
        try:
            self.conn.execute(ins)
        except IntegrityError:
            pass

    def read(self, username):
        """ get all groups from a user
        """
        s = select([groups.c.groupname])\
            .where(groups.c.group_id == usersgroups.c.group_id)\
            .where(usersgroups.c.user_id == users.c.user_id)\
            .where(users.c.username == username)
        g = self.conn.execute(s).fetchall()
        return rows2dict(s, g)

    def delete(self, username, groupname):
        """ add user in group
        """
        uid = select([users.c.user_id]).where(users.c.username == username)
        gid = select([groups.c.group_id]).where(
            groups.c.groupname == groupname)
        d = usersgroups.delete()\
                       .where(usersgroups.c.user_id == uid)\
                       .where(usersgroups.c.group_id == gid)
        try:
            self.conn.execute(d)
        except IntegrityError:
            pass


class Followers():

    def __init__(self, conn):
        self.conn = conn
        self.lastrowid = -1

    def create(self, who_id, whom_id):
        """ create a entry in table
        """
        try:
            ins = followers.insert().values(who_id=who_id,
                                            whom_id=whom_id)
            row = self.conn.execute(ins)
            self.lastrowid = row.inserted_primary_key
        except IntegrityError:
            # already exist not an error
            pass

    def read_graph_out(self, user_id):
        """ return the list of people you follow
        """
        s = select([users.c.username])\
            .where(followers.c.who_id == user_id)\
            .where(followers.c.whom_id == users.c.user_id)
        all = self.conn.execute(s).fetchall()
        return all

    def read_graph_in(self, user_id):
        """ return the list of people which follow you
        """
        s = select([users.c.username])\
            .where(followers.c.whom_id == user_id)\
            .where(followers.c.who_id == users.c.user_id)
        all = self.conn.execute(s).fetchall()
        return all

    def delete(self, who_id, whom_id):
        """ delete an entry or a list of entries
        both keys can not be None at the same time
        """
        d = delete([followers])
        if who_id is not None:
            d = d.where(followers.c.who_id == who_id)
        if whom_id is not None:
            d = d.where(followers.c.whom_id == whom_id)
        self.conn.execute(d)
        self.lastrowid = -1
