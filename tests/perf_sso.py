#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
import random
import json
from collections import Counter
from locust import HttpLocust, TaskSet, task

users = Counter()

headers = { 'Content-Type': 'application/json' }

class UserBehavior(TaskSet):

    def _log_as_admin(self):
        # log as admin if group do not exist
        data = {
            'username': 'admin',
            'password': 'admin2013'
        }
        with self.client.post('/login', 
                              data=json.dumps(data), 
                              headers=headers,
                              catch_response=True) as resp:
            if resp.status_code is not 200:
                resp.failure()
            else:
                resp.success()

    def _check_group(self, groupname):
        """ check if group and if not create it """
        with self.client.get('/groups/{0}'.format(groupname), 
                             headers=headers,
                             name='/groups/[groupname]',
                             catch_response=True) as resp1:
            if resp1.status_code == 404:
                print 'try to create a new group'
                with self.client.post('/groups/{0}'.format(self.groupname), 
                                      name='/groups/[groupname]',
                                      headers=headers,
                                      catch_response=True) as resp2:
                    if resp2.status_code is not 200:
                        msg = resp2.json()
                        print 'Creating group {0} failed with status {1}: {2}'\
                            .format(self.groupname,
                                    resp2.status_code,
                                    msg['error'])
                        resp2.failure()
                    else:
                        resp2.success()
            resp1.success()

    def _add_user_group(self, user_id):
        """ add user in group """
        with self.client.post('/profile/{0}/group/{1}'.format(self.username,
                                                              self.groupname),
                              name='/profile/[username]/group/[groupname]',
                              headers=headers,
                              catch_response=True) as resp:
            if resp.status_code is 200:
                resp.success()
            else:
                resp.failure('call to /profile/*/group/* failed with http code {0}'\
                             .format(resp.status_code))

    def _add_user_links(self, user_id):
        # add links
        extracted = [f for f in users.elements() \
                     if random.randint(0,100) % 9 == 0]
        for f in extracted:
            with self.client.post('/followers/user{0}'.format(f),
                                  name='/followers/[username]',
                                  headers=headers,
                                  catch_response=True) as resp:
                if resp.status_code is 200:
                    resp.success()
                else:
                    resp.failure('call to /followers failed with http code {0}'\
                                 .format(resp.status_code))

    def _create_user(self, user_id):
        # user data
        data = {
            'username': self.username,
            'password': 'pwd{0}'.format(user_id),
            'password2': 'pwd{0}'.format(user_id),
            'email': '{0}@nowhere.com'.format(user_id),
        }
        with self.client.post('/profile', 
                              data=json.dumps(data), 
                              headers=headers,
                              catch_response=True) as resp:
            if resp.status_code is not 200:
                resp.failure('Create user via post /profile failed with http code {0}'\
                             .format(resp.status_code))

    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        user_id = random.randint(100000, 999999)
        self.username = 'user{0}'.format(user_id)
        self.groupname = 'group{0}'.format((user_id-100000)%10)
        # create user if not exist
        if user_id not in users:
            self._log_as_admin()
            self._check_group(self.groupname)
            self._create_user(user_id)
            self._add_user_group(user_id)
            self._add_user_links(user_id)

        # keep a trace of who has been created
        users[user_id] = 1

        # log user
        data = {
            'username': self.username,
            'password': 'pwd{0}'.format(user_id),
        }
        # print 'try to log in as user {0}'.format(user_id)
        with self.client.post('/login', 
                              data=json.dumps(data), 
                              headers=headers,
                              catch_response=True) as resp:
            if resp.status_code is 200:
                resp.success()
            else:
                resp.failure('call to /login failed with http code {0}'\
                             .format(resp.status_code))

    @task(20)
    def get_profile(self):
        # print 'get profile'
        with self.client.get('/profile',
                             catch_response=True) as resp:
            # print resp.status_code
            # print resp.content
            resp.close()
            resp.success()

    @task(10)
    def get_login(self):
        # print 'check if loggued'
        with self.client.get('/login',
                             catch_response=True) as resp:
            # print resp.status_code
            # print resp.content
            resp.close()
            resp.success()

    @task(1)
    def get_groups(self):
        # print 'check if loggued'
        with self.client.get('/profile/{0}/groups'.format(self.username),
                             name='/profile/[username]/groups',
                             catch_response=True) as resp:
            # print resp.status_code
            # print resp.content
            resp.close()
            resp.success()


    @task(1)
    def get_followers_in(self):
        # print 'check if loggued'
        with self.client.get('/followers/in'.format(self.username),
                             catch_response=True) as resp:
            # print resp.status_code
            # print resp.content
            resp.close()
            resp.success()


    @task(1)
    def get_followers_out(self):
        # print 'check if loggued'
        with self.client.get('/followers/out'.format(self.username),
                             catch_response=True) as resp:
            # print resp.status_code
            # print resp.content
            resp.close()
            resp.success()


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 10
    max_wait = 150
