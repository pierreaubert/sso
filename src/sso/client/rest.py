#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import json
import requests
from sso.accesscontrol.exceptions import InternalServerError, Unauthorized


class Client(requests.Session):

    def __init__(self, sso_url, logger):
        """ record basic fields
        """
        # super().__init__(self)
        requests.Session.__init__(self)
        self.sso_url = sso_url
        self.headers.update({ 'Content-Type': 'application/json' })
        self.logger = logger

    def post_login(self, username, password, short=True):
        error = None
        data = {
            'username': username,
            'password': password,
        }
        if short is False:
            data['longtime'] = True
            
        resp = self.post(self.sso_url + '/login', data=json.dumps(data))
        try:
            status = resp.json()
        except Exception as jse:
            raise InternalServerError(jse.__str__())

        if resp.status_code != requests.codes.ok: 
            if 'status' in status:
                if 'error' in status:
                    error = status['error']
                    raise Unauthorized(error)
                else:
                    self.logger.error('sso/login failed without error msg')
                    raise Unauthorized()
            else:
                raise InternalServerError('no status in server answer')
        # print '(debug) cookie={0}'.format(resp.cookies['ssosign'])


    def post_profile(self, new_username, new_password, new_email):
        """ create a new user """
        error = None
        data = {
            'username': new_username,
            'password': new_password,
            'password2': new_password,
            'email': new_email
        }
        resp = self.post(self.sso_url + '/profile', data=json.dumps(data))
        try:
            status = resp.json()
        except Exception as jse:
            raise InternalServerError(jse.__str__())

        if resp.status_code != requests.codes.ok: 
            if 'status' in status:
                if 'error' in status:
                    error = status['error']
                    raise Unauthorized(error)
                else:
                    self.logger.error('sso/login failed without error msg')
                    raise Unauthorized()
            else:
                raise InternalServerError('no status in server answer')
        # print '(debug) cookie={0}'.format(resp.cookies['ssosign'])


    def get_login(self):
        """ return True if correcty authenticated or False

        """
        resp = self.get(self.sso_url + '/login')
        if resp.status_code != 200:
            data = resp.json()
            if 'status' in data:
                status = resp.json()
                if 'error' in status:
                    error = status['error']
                    raise Unauthorized(error)
                else:
                    self.logger.error('sso/login failed without error msg')
                    raise Unauthorized()
        # print '(debug) cookie={0}'.format(resp.cookies['ssosign'])

    def get_profile(self):
        """ return user profile if correcty authenticated or None
        """
        resp = self.get(self.sso_url + '/profile')
        if resp.status_code != requests.codes.ok:
            try:
                data = resp.json()
                if 'status' in data:
                    status = resp.json()
                    if 'error' in status:
                        error = status['error']
                        raise Unauthorized(error)
                    else:
                        self.logger.error('sso/login failed without error msg')
                        raise Unauthorized()
            except ValueError as ve:
                error = 'bug: {0}'.format(ve)
                raise Unauthorized(error)

        profile = resp.json()
        if 'users' in profile:
            # print '(debug) cookie={0}'.format(resp.cookies['ssosign'])
            return profile['users']
        # we should not go here
        raise InternalServerError()

    def get_userid(self):
        """ return user_id
        """
        profile = self.get_profile()
        if profile is not None and 'user_id' in profile:
            return profile['user_id']
        return None

    def get_username(self):
        """ return user_id
        """
        profile = self.get_profile()
        if profile is not None and 'username' in profile:
            return profile['username']
        return None

    def is_allowed(self, rules):
        """ return true if user is allowed to pass rules 

        this is not usefull from an external application. Use it like:
        app 'sso' on server1
        app 'checker' on server2
        'user' want to access an api on 'checker'
        'checker' can check the 'user' is authenticated via the cookie 
        but the cookie do not hold all informations about the 'user' 
        thus checker need to ask 'sso' if user satisfy the 'rule'

        remember the call carried the 'rule' thus it is easy to lie to sso
        """
        data = {
            'ac': str(rules)
        }
        resp = self.post(self.sso_url + '/accesscontrol', data=json.dumps(data))
        # print '(debug) cookie={0}'.format(resp.cookies['ssosign'])
        try:
            resp.json()
        except Exception:
            return False

        if resp.status_code != requests.codes.ok: 
            return False
        return True


        
        
        


