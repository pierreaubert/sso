#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
import os
import unittest
import json
from exceptions import IOError
#from werkzeug.test import EnvironBuilder
from sso.appsso import create_sso
from sso.accesscontrol.ac import  AC_Not, \
    AC_UserGroup, AC_User


class SSOTests(unittest.TestCase):

    def setUp(self):
        self.server = create_sso()
        self.server.testing = True
        self.headers = { 'Content-Type': 'application/json' }

    def tearDown(self):
        try:
            fname = self.server.config['DATABASE_URI']
            pos = fname.find('://')
            name = fname[pos+3:]
            if os.access(name, os.F_OK):
                os.unlink(name)
            else:
                print '''Can't open/close db file!'''
        except IOError:
            print '''Can't open/close db file!'''

    def test_empty_db(self):
        """ let see if the schema as been uploaded
        """
        with self.server.test_client() as tc:
            jstats = tc.get('/login')
            assert jstats.status_code == 401

    def register(self, tc, N):
        """ register user N
        """
        data = {
            'username': 'pierre{0}'.format(N),
            'password': 'toto{0}'.format(N),
            'password2': 'toto{0}'.format(N),
            'email': 'pierre{0}@toto.com'.format(N)
        }
        return tc.post('/profile',
                       data=json.dumps(data),
                       content_type='application/json')

    def login_admin_ok(self, tc):
        data = {
            'username': 'admin',
            'password': 'admin2013',
        }
        return tc.post('/login', data=data)

    def login1_ok(self, tc):
        data = {
            'username': 'pierre1',
            'password': 'toto1',
        }
        return tc.post('/login', data=data)

    def login1_ko_wrong_password(self, tc):
        data = {
            'username': 'pierre1',
            'password': 'toto2',
        }
        return tc.post('/login', data=data)

    def login1_ko_wrong_user(self, tc):
        data = {
            'username': 'pierre1',
            'password': 'toto2',
        }
        return tc.post('/login', data=data)

    def login2_ok(self, tc):
        data = {
            'username': 'pierre2',
            'password': 'toto2',
        }
        return tc.post('/login', data=data)

    def register3_failed_passwords(self, tc):
        """ register user 3, failed with != passwords
        """
        data = {
            'username': 'pierre1',
            'password': 'toto1',
            'password2': 'tto1',
            'email': 'pierre1@toto.com'
        }
        return tc.post('/profile', data=data)

    def register4_failed_password(self, tc):
        """ register user 3, failed miss a password
        """
        data = {
            'username': 'pierre1',
            'password2': 'toto1',
            'email': 'pierre1@toto.com'
        }
        return tc.post('/profile', data=data)

    def register5_failed_email(self, tc):
        """ register user 3, failed email already taken
        """
        data = {
            'username': 'pierre5',
            'password': 'toto1',
            'password2': 'toto1',
            'email': 'pierre1@toto.com'
        }
        return tc.post('/profile', data=data)

    def test_login1(self):
        with self.server.test_client() as tc:
            jstats = tc.get('/login')
            assert jstats.status_code == 401
            # print 'DEBUG: '+jstats.data
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'

    def test_register1(self):
        """ register 1 user
        """
        with self.server.test_client() as tc:
            jstats = self.register(tc, 1)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'

    def test_register2(self):
        """ register 2x the same user
        """
        with self.server.test_client() as tc:
            jstats = self.register(tc, 1)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            jstats = self.register(tc, 1)
            assert jstats.status_code == 400
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'

    def test_register4(self):
        """ register 2 users
        """
        with self.server.test_client() as tc:
            jstats = self.register(tc, 1)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            jstats = self.register(tc, 2)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'

    def test_register_ko_failed1(self):
        """ register ko
        """
        with self.server.test_client() as tc:
            jstats = self.register3_failed_passwords(tc)
            assert jstats.status_code == 400
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'

    def test_register_ko_failed2(self):
        """ register ko
        """
        with self.server.test_client() as tc:
            jstats = self.register4_failed_password(tc)
            assert jstats.status_code == 400
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'

    def test_register_ko_failed3(self):
        """ register ko
        """
        with self.server.test_client() as tc:
            jstats = self.register(tc, 1)
            jstats = self.register5_failed_email(tc)
            assert jstats.status_code == 400
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'

    def build_headers_from_cookie(self, response):
        cookies = response.headers.getlist('Set-Cookie')
        # build headers
        headers = []
        for cookie in cookies:
            header = ('Cookie', cookie)
            headers.append(header)
        return headers

    def follow_ok(self, tc, u2):
        return tc.post('/followers/pierre{0}'.format(u2))

    def follow_in(self, tc):
        return tc.get('/followers/in')

    def follow_out(self, tc):
        return tc.get('/followers/out')

    def test_scenario1(self):
        """ nominal scenario
        """
        with self.server.test_client() as tc:
            # register user 1
            jstats = self.register(tc, 1)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # log user 1
            jstats = self.login1_ok(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # TODO make it work
            # tc do not keep track of cookies
            #assert 'ssosign' in jstats.cookies
            #if 'ssosign' in request.cookies:
            #    print '>1 ' + request.cookies['ssosign']
            #print '>1 ' + jstats.headers['Set-Cookie']
            headers = self.build_headers_from_cookie(jstats)
            # check if logged
            jstats = tc.get('/login', headers=headers)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # TODO make it work
            # tc do not keep track of cookies
            #assert 'ssosign' in request.cookies
            #if 'ssosign' in request.cookies:
            #    print '>2 ' + request.cookies['ssosign']
            #print '>2 ' + jstats.headers['Set-Cookie']
            # check if logged (another time)
            jstats = tc.get('/login', headers=headers)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            #assert 'ssosign' in request.cookies
            #if 'ssosign' in request.cookies:
            #    print '>3 ' + request.cookies['ssosign']
            #print '>3 ' + jstats.headers['Set-Cookie']
            # logout
            jstats = tc.delete('/login')
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            #assert 'ssosign' not in request.cookies
            # check if logged (must failed)
            jstats = tc.get('/login')
            assert jstats.status_code == 401
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'
            #assert 'ssosign' not in request.cookies
            #print '>4 ' + jstats.headers['Set-Cookie']

    def test_scenario2(self):
        """ forget to login
        """
        with self.server.test_client() as tc:
            # register user 1
            jstats = self.register(tc, 1)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # check if logged (must failed)
            jstats = tc.get('/login')
            assert jstats.status_code == 401
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'
            #if 'ssosign' in request.cookies:
            #    print '>1 ' + request.cookies['ssosign']
            # log user 1
            jstats = self.login1_ok(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            #assert 'ssosign' in request.cookies
            #if 'ssosign' in request.cookies:
            #    print '>2 ' + request.cookies['ssosign']
            headers = self.build_headers_from_cookie(jstats)
            # check if logged
            jstats = tc.get('/login', headers=headers)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            #if 'ssosign' in request.cookies:
            #    print '>3 ' + request.cookies['ssosign']

    def test_scenario3(self):
        """ user do not exist
        """
        with self.server.test_client() as tc:
            # check if logged (must failed)
            jstats = tc.get('/login')
            assert jstats.status_code == 401
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'
            # log user 1
            jstats = self.login1_ok(tc)
            assert jstats.status_code == 401
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'
            # check if logged
            jstats = tc.get('/login')
            assert jstats.status_code == 401
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'

    def _scenario4(self,  tc1,  tc2):
        """ 2 clients
        """
        # register 1
        jstats = self.register(tc1, 1)
        assert jstats.status_code == 200
        stats = json.loads(jstats.data)
        assert stats['status'] == 'ok'
        # log user 1
        jstats = self.login1_ok(tc1)
        assert jstats.status_code == 200
        stats = json.loads(jstats.data)
        assert stats['status'] == 'ok'
        headers1 = self.build_headers_from_cookie(jstats)
        #if 'ssosign' in request.cookies:
        #    print '>1 ' + request.cookies['ssosign']
        # check if logged
        jstats = tc1.get('/login', headers=headers1)
        assert jstats.status_code == 200
        stats = json.loads(jstats.data)
        assert stats['status'] == 'ok'
        #if 'ssosign' in request.cookies:
        #    print '>2 ' + request.cookies['ssosign']
        # register 2
        jstats = self.register(tc2, 2)
        assert jstats.status_code == 200
        stats = json.loads(jstats.data)
        assert stats['status'] == 'ok'
        # log user 2
        jstats = self.login2_ok(tc2)
        assert jstats.status_code == 200
        stats = json.loads(jstats.data)
        assert stats['status'] == 'ok'
        headers2 = self.build_headers_from_cookie(jstats)
        #if 'ssosign' in request.cookies:
        #    print '>3 ' + request.cookies['ssosign']
        # check if logged
        jstats = tc2.get('/login', headers=headers2)
        assert jstats.status_code == 200
        stats = json.loads(jstats.data)
        assert stats['status'] == 'ok'
        #if 'ssosign' in request.cookies:
        #    print '>4 ' + request.cookies['ssosign']

    def test_scenario4(self):
        with self.server.test_client() as tc1:
            self._scenario4(tc1, tc1)

    def test_scenario5(self):
        """ user do not exist
        """
        with self.server.test_client() as tc:
            # register 1
            jstats = self.register(tc, 1)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # register 2
            jstats = self.register(tc, 2)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # register 3
            jstats = self.register(tc, 3)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # log user 1
            jstats = self.login1_ok(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # 1 follow 2
            # graph is
            #     1 -> 2
            jstats = self.follow_ok(tc, 2)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # 1 follow 3
            # graph is
            #     1 -> 2
            #     1 -> 3
            jstats = self.follow_ok(tc, 3)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # get graph in for 1
            jstats = self.follow_out(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            assert stats['graph']['node'] == 'pierre1'
            assert 'pierre2' in stats['graph']['out']
            assert 'pierre3' in stats['graph']['out']
            # log user 2
            jstats = self.login2_ok(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # 2 follow 3
            # graph is
            #     1 -> 2
            #     1 -> 3
            #     2 -> 3
            jstats = self.follow_ok(tc, 3)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # check if log user 2 is still loggued
            jstats = tc.get('/login')
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # get graph in for 2
            # graph is
            #     1 -> 2
            #     1 -> 3
            #     2 -> 3
            jstats = self.follow_in(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            # print stats
            assert stats['status'] == 'ok'
            assert stats['graph']['node'] == 'pierre2'
            assert 'pierre1' in stats['graph']['in']
            # check if log user 2 is still loggued
            jstats = tc.get('/login')
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # get graph out for 2
            # graph is
            #     1 -> 2
            #     1 -> 3
            #     2 -> 3
            jstats = self.follow_out(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            assert stats['graph']['node'] == 'pierre2'
            assert 'pierre3' in stats['graph']['out']
            # check if log user 2 is still loggued
            jstats = tc.get('/login')
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'

    def test_scenario6(self):
        """ check access control for groups
        """
        with self.server.test_client() as tc:
            # register 1
            jstats = self.register(tc, 1)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # log user 1
            jstats = self.login1_ok(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # user 1 can read groups
            jstats = tc.get('/groups')
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            assert 'groups' in stats
            assert 'admin' in stats['groups'][0]['groupname']
            # user 1 can read a specific groups
            jstats = tc.get('/groups/users')
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            assert 'groups' in stats
            assert 'users' in stats['groups']['groupname']
            # user 1 can't create groups
            jstats = tc.post('/groups/grouptest')
            assert jstats.status_code == 403
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ko'

    def test_scenario7(self):
        """ check access control for groups with user admin
        """
        with self.server.test_client() as tc:
            # log user admin
            jstats = self.login_admin_ok(tc)
            self.assertEqual(jstats.status_code, 200)
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # check user admin is in admin group
            jstats = tc.get('/groups')
            self.assertEqual(jstats.status_code, 200)
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            assert 'groups' in stats
            assert 'admin' in stats['groups'][0]['groupname']
            # user admin can create groups
            #jstats = tc.post('/groups/grouptest')
            #self.assertEqual(jstats.status_code, 200)
            #stats = json.loads(jstats.data)
            #assert stats['status'] == 'ok'

    def test_scenario8(self):
        """ check access control for groups with user pierre
        but with level admin
        """
        with self.server.test_client() as tc:
            # register 1
            jstats = self.register(tc, 1)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # log user admin
            jstats = self.login_admin_ok(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # add user 1 in admin group
            jstats = tc.post('/profile/pierre1/group/admin')
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # log user 1
            jstats = self.login1_ok(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # user 1 can read groups
            jstats = tc.get('/groups')
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            assert 'groups' in stats
            assert 'admin' in stats['groups'][0]['groupname']
            # user 1 can know create groups
            jstats = tc.post('/groups/grouptest')
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'

    def _check_rule(self, tc, rule):
        data = {
            'ac': str(rule),
        }
        jstats = tc.post('/accesscontrol', 
                         data=json.dumps(data), 
                         content_type='application/json')
        return jstats.status_code


    def test_scenario9(self):
        """ check rules (de)serial
        """
        with self.server.test_client() as tc:
            # register 1
            jstats = self.register(tc, 1)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # log user admin
            jstats = self.login_admin_ok(tc)
            assert jstats.status_code == 200
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # check that user is 'admin'
            r1 = AC_User('admin')
            data = {
                'ac': str(r1),
            }
            # test get
            jstats = tc.get('/accesscontrol?ac={0}'.format(r1))
            self.assertEqual(jstats.status_code, 200)
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # test post
            jstats = tc.post('/accesscontrol', data=data)
            self.assertEqual(jstats.status_code, 200)
            stats = json.loads(jstats.data)
            assert stats['status'] == 'ok'
            # a few check
            self.assertEqual(200, self._check_rule(tc, AC_User('admin')))
            self.assertEqual(403, self._check_rule(tc, AC_User('user1')))
            self.assertEqual(403, self._check_rule(tc, AC_Not(AC_User('admin'))))
            self.assertEqual(200, self._check_rule(tc, AC_UserGroup('admin')))
            self.assertEqual(403, self._check_rule(tc, AC_UserGroup('user')))
            


        
if __name__ == '__main__':
    unittest.main()
