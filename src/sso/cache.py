#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------

from flask import g, current_app
from accesscontrol.exceptions import Unauthorized


def cache_profile_in_g():
    """ set user_id, username and email if any in g
    not the best way and i would like to put it to redis / memcache in
    order to remove bandwidth between mobile and server
    """
    print 'coucou from cache profile in g'
    user_id = getattr(g, 'user_id', None)
    if user_id is None:
        profile = current_app.sso_client.get_profile()
        print '> profile'
        if profile is not None:
            print '> caching profile'
            user_id = profile['user_id']
            g.user_id = user_id
            g.username = profile['username']
            g.email = profile['email']

    if user_id is None:
        raise Unauthorized('unknown user')
