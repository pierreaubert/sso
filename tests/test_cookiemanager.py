#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
import time
import unittest

from sso.cookiemanager import CookieManager


class CookieManagerTests(unittest.TestCase):

    def setUp(self):
        # be careful when debugging cookie is valid 2s by default
        self.cm = CookieManager(
            'secret must be a long random key',
            2,
            50,
            'testsso',
            '/test',
            5
        )

    def tearDown(self):
        pass

    def test_cookie_encrypt_decrypt(self):
        """
        """
        cookie = 'pierre~192.168.1.12~s~102910290.43~3~$'
        enc = self.cm.encode(cookie)
        dec = self.cm.decode(enc)
        assert cookie == dec

    def test_cookie_check_attributes(self):
        """
        """
        cookie = 'pierre~192.168.1.12~s~102910290.43~3~$'
        enc = self.cm.encode(cookie)
        usr = self.cm.get_username(enc)
        assert usr == 'pierre'
        ip = self.cm.get_ip(enc)
        assert ip == '192.168.1.12'
        seconds = self.cm.get_seconds(enc)
        assert seconds == self.cm.short_seconds
        counter = self.cm.get_counter(enc)
        assert counter == '3'

    def test_cookie_isvalid(self):
        """
        """
        username = 'pierre'
        ip = '192.168.10.109'
        short = True
        cookie = self.cm.create(username, ip, short)
        valid, newcookie, msg = self.cm.is_valid(cookie, ip)
        assert valid is True
        assert newcookie is not None
        ip2 = '192.168.10.110'
        valid, newcookie, msg = self.cm.is_valid(cookie, ip2)
        assert valid is False
        assert newcookie is None

    def test_cookie_counter(self):
        """
        """
        username = 'pierre'
        ip = '192.168.10.109'
        short = True
        cookie = self.cm.create(username, ip, short)
        valid = True
        for i in range(0, 5):
            valid, cookie, msg = self.cm.is_valid(cookie, ip)
            assert valid is True
        valid, cookie, msg = self.cm.is_valid(cookie, ip)
        assert valid is False

    def test_cookie_timeout(self):
        """
        """
        username = 'pierre'
        ip = '192.168.10.109'
        short = True
        cookie = self.cm.create(username, ip, short)
        valid, newcookie, msg = self.cm.is_valid(cookie, ip)
        assert valid is True
        assert newcookie is not None
        time.sleep(self.cm.short_seconds+1)
        valid, newcookie, msg = self.cm.is_valid(cookie, ip)
        assert valid is False
        assert newcookie is None


if __name__ == '__main__':
    unittest.main()
