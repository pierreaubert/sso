#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
from sso.appsso import create_sso

# create the application for WSGI server
app = create_sso()


#@app.errorhandler(401)
#def sso_401(error):
#    # put json here
#    data = {
#        'status': 'ko',
#        'error': 'Unauthorized'
#    }
#    resp = make_response(json.loads(data), 401)
#    resp.headers['mimetype'] = 'application/json'
#    return resp


# nice for debugging
if __name__ == '__main__':
    app.run(host=app.config['SSO_HOST'],
            port=app.config['SSO_PORT'],
            debug=app.config['DEBUG'])
