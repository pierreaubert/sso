#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# 7pi/sso a Single Sign On server
# Copyright (C) 2012-2015 Pierre Aubert pierreaubert(at)yahoo(dot)fr
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ----------------------------------------------------------------------
# get helpers
from sso.helpers import JsonHTTPException

class InternalServerError(JsonHTTPException):
    """ a class to manage http 500 i.e. Internal Server Error """
    def __init__(self, description=None, response=None):
        JsonHTTPException.__init__(self, description)
        self.code = 500
        self.description = '500: Internal Server Error'
        if description is not None:
            self.description += ' (' + description + ')'


class Unauthorized(JsonHTTPException):
    """ a class to manage http 401 i.e. Unauthorized """
    def __init__(self, description=None, response=None):
        JsonHTTPException.__init__(self, description)
        self.code = 401
        self.description = '401: Unauthorized'
        if description is not None:
            self.description += ' (' + description + ')'
        
    def get_headers(self, environ=None):
        """ return headers

        :param environ wsgi environ
        :return a list of http headers
        """
        return [('Content-Type', 'text/json'),
                ('Set-Cookie', 'name=ssosign; Expires=0')]


class Forbidden(JsonHTTPException):
    """ a class to manage http 403 i.e. Forbidden """
    def __init__(self, description=None, response=None):
        JsonHTTPException.__init__(self, description)
        self.code = 403
        self.description = '403: Forbidden'
        if description is not None:
            self.description += ' (' + description + ')'


class NotFound(JsonHTTPException):
    """ a class to manage http 404 i.e. NotFound """
    def __init__(self, description=None, response=None):
        JsonHTTPException.__init__(self, description)
        self.code = 404
        self.description = '404: Not Found'
        if description is not None:
            self.description += ' (' + description + ')'


class BadRequest(JsonHTTPException):
    """ a class to manage http 400 i.e. BadRequest """
    def __init__(self, description=None, response=None):
        JsonHTTPException.__init__(self, description)
        self.code = 400
        self.description = '400: Bad Request'
        if description is not None:
            self.description += ' (' + description + ')'

